primitive dff_prim (
    output reg q_out,
    input  clock, data
);

    table
    //  clk  data : state : q_out
        (01) 0    : ?     : 0; // (01): rising edge
        (01) 1    : ?     : 1;
        (0x) 1    : 1     : 1;
        (0x) 0    : 0     : 0;

        (?0) ?    : ?     : -;
        ?    (??) : ?     : -;
    endtable


endprimitive
