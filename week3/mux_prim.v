primitive mux_prim (
    output mux_out,
    input  select,
    input  a,
    input  b
);

    // table
    // //  select  a  b : mux_out
    //     0       0  0 : 0;
    //     0       0  1 : 0;
    //     0       1  0 : 1;
    //     0       1  1 : 1;
    //     1       0  0 : 0;
    //     1       0  1 : 1;
    //     1       1  0 : 0;
    //     1       1  1 : 1;

    // // Part 2: with unknowns
    //     0       0  x : 0;
    //     0       1  x : 1;
    //     1       x  0 : 0;
    //     1       x  1 : 1;

    //     x       0  0 : 0;
    //     x       1  1 : 1;
    // endtable


    // Part 3: condense with ?
    // ? = (0,1, or x)
    table
    //  select  a  b : mux_out
        0       0  ? : 0;
        0       1  ? : 1;
        1       ?  0 : 0;
        1       ?  1 : 1;

        ?       0  0 : 0;
        ?       1  1 : 1;
    endtable

endprimitive
