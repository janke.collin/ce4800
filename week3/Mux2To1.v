module Mux2To1 (
    output mux_out,
    input  select,
    input  [1:0] mux_in
);

    mux_prim(mux_out, select, mux_in[0], mux_in[1]);

endmodule
