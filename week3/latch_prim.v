primitive latch_prim (
    output reg q_out,
    input enable, data
);

    table
    // enable data : state : q_out
        1     1    : ?     : 1;
        1     0    : ?     : 0;
        0     ?    : ?     : -; // -: previous output

        x     0    : 0     : -;
        x     1    : 1     : -;
    endtable

endprimitive
