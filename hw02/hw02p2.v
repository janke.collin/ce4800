primitive hw02p2 (
    output y,
    input a, b, c, d
);

    table
    //  a b c d : y
        0 0 0 0 : 0;
        0 0 0 1 : 0;
        0 0 1 0 : 0;
        0 0 1 1 : 0;
        0 1 0 0 : 0;
        0 1 0 1 : 0;
        0 1 1 0 : 1;
        0 1 1 1 : 0;
        1 0 0 0 : 0;
        1 0 0 1 : 0;
        1 0 1 0 : 0;
        1 0 1 1 : 0;
        1 1 0 0 : 0;
        1 1 0 1 : 0;
        1 1 1 0 : 0;
        1 1 1 1 : 0;
    endtable

endprimitive
