module hw02p1 (
    output y,
    input a, b, c, d
);

    wire w1, w1_n, d_n, w2;

    or(w1, a, d);
    not(w1_n, w1);
    not(d_n, d);
    and(w2, b, c, d_n);
    and(y, w1_n, w2);

endmodule
