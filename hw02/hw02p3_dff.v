primitive hw02p3_dff (
    output reg q,
    input clk, rst_n, en_n, d
);
    table
    //  clk  rst_n  en_n  d : cs : q
        r    1      0     0 : ?  : 0;
        r    1      0     1 : ?  : 1;
        f    1      ?     ? : ?  : -;
        ?    1      ?     * : ?  : -;

        // Reset
        ?    0      ?     ? : ?  : 0;
        ?    *      ?     ? : ?  : 0;

        // Enable
        r    1      1     ? : ?  : -;
        ?    1      *     ? : ?  : -;
    endtable
endprimitive
