module hw02p1_Test ();

    wire y;
    reg a;
    reg b;
    reg c;
    reg d;

    hw02p1 uut (
        .y(y),
        .a(a),
        .b(b),
        .c(c),
        .d(d)
    );

    initial $monitor("%b %b %b %b : %b", a, b, c, d, y);

    initial #16 $finish;

    initial begin
           a = 0; b = 0; c = 0; d = 0;
        #1 a = 0; b = 0; c = 0; d = 1;
        #1 a = 0; b = 0; c = 1; d = 0;
        #1 a = 0; b = 0; c = 1; d = 1;
        #1 a = 0; b = 1; c = 0; d = 0;
        #1 a = 0; b = 1; c = 0; d = 1;
        #1 a = 0; b = 1; c = 1; d = 0;
        #1 a = 0; b = 1; c = 1; d = 1;
        #1 a = 1; b = 0; c = 0; d = 0;
        #1 a = 1; b = 0; c = 0; d = 1;
        #1 a = 1; b = 0; c = 1; d = 0;
        #1 a = 1; b = 0; c = 1; d = 1;
        #1 a = 1; b = 1; c = 0; d = 0;
        #1 a = 1; b = 1; c = 0; d = 1;
        #1 a = 1; b = 1; c = 1; d = 0;
        #1 a = 1; b = 1; c = 1; d = 1;
    end

endmodule
