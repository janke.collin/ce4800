module hw02p4 (
    output clk_by_11,
    input  clk, rst_b
);
    wire clk_by_11_int, clk_by_11_n;
    wire [3:0] q;
    wire [2:1] w;

    assign clk_by_11_n = ~clk_by_11_int;
    assign clk_by_11 = clk_by_11_n;

    //          q,    clk,  rst_n,       t
    hw02p4_tff(q[0], clk,  clk_by_11_n, 1);
    hw02p4_tff(q[1], q[0], clk_by_11_n, 1);
    hw02p4_tff(q[2], q[1], clk_by_11_n, 1);
    hw02p4_tff(q[3], q[2], clk_by_11_n, 1);

    assign w[1] = ~q[0] & q[1] & q[2] & ~q[3];
    assign w[2] = w[1] | clk_by_11_int;

    hw02p4_tff(clk_by_11_int, clk, rst_b, w[2]);

endmodule
