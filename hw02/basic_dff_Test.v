module basic_dff_Test ();

    wire q;
    reg clk;
    reg d;

    basic_dff uut (q, clk, d);

    initial $monitor("[%2t] clk=%b %b-%s->%b",
        $time, clk, d, (clk ? "-" : " "), q
    );

    initial #60 $finish;

    always begin
        #2 clk = 1;
        #2 clk = 0;
    end

    always begin
        #4 d = 1;
        #4 d = 0;
    end

endmodule
