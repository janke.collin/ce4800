module hw02p4_tff_Test ();

    wire q;
    reg clk;
    reg rst_n;
    reg t;

    hw02p4_tff uut (q, clk, rst_n, t);

    initial $monitor("[%2t] clk=%b rst_n=%b t=%b q=%b",
        $time, clk, rst_n, t, q
    );

    always begin
        #2 clk = 1;
        #2 clk = 0;
    end

    initial begin
        clk = 0; rst_n = 0; t = 0;
        #3 rst_n = 1;       // high
        #1 assert(q === 1); // falling edge, reset to 1
        #1 assert(q === 1); // low, no toggle
        #1 assert(q === 1); // rising edge, no toggle
        #1 assert(q === 1); // high, no toggle
        #2 t = 1;           // low, enable toggle
        #2 assert(q === 0); // high, toggled
        #4 assert(q === 1); // high, toggled
        #4 assert(q === 0); // high, toggled
        #4 assert(q === 1); // high, toggled
        #1 rst_n = 0;       // falling edge, enable rst
        #1 assert(q === 1); // low, reset to 1
        #1 assert(q === 1); // rising edge, reset to 1
        #1 assert(q === 1); // high, reset to 1
        #1 rst_n = 1;       // falling edge, disable toggle
        #1 assert(q === 1); // low, still 1
        #2 assert(q === 0); // high, toggled

        #2 $finish;
    end

endmodule
