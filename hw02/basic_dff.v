primitive basic_dff (
    output reg q,
    input clk, d
);

    table
    //  clk  d : cs : q
        r    0 : ?  : 0;
        r    1 : ?  : 1;
        f    ? : ?  : -;
        ?    * : ?  : -;
    endtable
endprimitive
