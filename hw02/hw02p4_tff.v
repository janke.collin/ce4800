primitive hw02p4_tff (
    output reg q,
    input clk, rst_n, t
);
    table
    //  clk  rst_n  t : cs : q
        r    1      0 : ?  : -;
        r    1      1 : 0  : 1;
        r    1      1 : 1  : 0;
        f    1      ? : ?  : -;
        ?    1      * : ?  : -;

        // Reset
        ?    0      ? : ?  : 1;
        ?    *      ? : ?  : 1;
    endtable
endprimitive
