module hw02p4_Test ();

    wire clk_by_11;
    reg clk;
    reg rst_b;

    hw02p4 uut (
        .clk_by_11(clk_by_11),
        .clk(clk),
        .rst_b(rst_b)
    );

    initial $monitor("[%2t] clk=%b clk_by_11=%b", $time, clk, clk_by_11);

    always begin
        #1 clk = 1;
        #1 clk = 0;
    end

    initial begin
           rst_b = 0;
        #2 assert(clk_by_11 === 0);
           rst_b = 1;
        #2  assert(clk_by_11 === 1); // 1
        #10 assert(clk_by_11 === 1); // 6
        #10 assert(clk_by_11 === 0); // 11
        #2  assert(clk_by_11 === 1); // 1
        #10 assert(clk_by_11 === 1); // 6
        #10 assert(clk_by_11 === 0); // 11
        #2  assert(clk_by_11 === 1); // 1
        #2 $finish;
    end

endmodule
