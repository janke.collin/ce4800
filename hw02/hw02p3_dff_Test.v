module hw02p3_dff_Test ();

    wire q;
    reg clk;
    reg rst_n;
    reg en_n;
    reg d;

    hw02p3_dff uut (q, clk, rst_n, en_n, d);

    initial $monitor("[%2t] clk=%b rst_n=%b en_n=%b %b-%s->%b",
        $time, clk, rst_n, en_n, d, (!clk || en_n ? " " : (rst_n ? "0" : "-")), q
    );

    always begin
        #2 clk = 1;
        #2 clk = 0;
    end

    initial begin
        clk = 0;
        en_n = 0; rst_n = 0; d = 0;
        // Basic (no enable or rst)
        #1 rst_n = 1;
        #2 assert(q === 0);
        #2 d = 1;
        #2 assert(q === 1);
        #4 assert(q === 1);

        // Async Reset
        #1 rst_n = 0;       // falling edge, enable rst
        #1 assert(q === 0); // low
        #1 assert(q === 0); // rising edge
        #1 assert(q === 0); // high
        #1 rst_n = 1;       // falling edge, disable rst
        #1 assert(q === 0); // low, still reset
        #2 assert(q === 1); // high, 1 applied

        // Enable + reset precedence
        #2 en_n = 1; d = 0; // low, disable
        #2 assert(q === 1); // high, still 1
        #2 rst_n = 0;       // low, enable rst
        #1 assert(q === 0); // falling edge, reset applied
        #1 assert(q === 0); // high, still reset
        #2 rst_n = 1; d = 1;// low, disable rst
        #2 assert(q === 0); // high, 1 not applied
        #2 en_n = 0;        // low, enable
        #2 assert(q === 1); // high, 1 applied
        #2 en_n = 1; d = 0; // low, disable
        #2 assert(q === 1); // high, 0 not applied
        #2 en_n = 0;        // low, enable
        #2 assert(q === 0); // high, 0 applied

        #2 $finish;
    end

endmodule
