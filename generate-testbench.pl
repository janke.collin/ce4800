#!/usr/bin/perl -w

my $testSuffix = "_Test";
my $indent = "    ";

sub trim {
    my @r = map { local $_ = $_; if ($_) { s/^\s+|\s+$//g; } $_ } @_;
    return (scalar @_ == 1) ? $r[0] : @r;
}

sub getModuleHeader {
    my $file = shift;

    my $fh;
    unless(open($fh, '<', $file)) {
        print STDERR "Failed to open $file: $!\n";
        next;
    }

    my ($module, $line, $isPrimitive);
    while (!$module && defined($line = readline($fh))) {
        chomp $line;
        $line =~ s/^\s+|\s+$//g;
        if ($line =~ /((module|primitive)\s+([\w\$]+))/) {
            $isPrimitive = ($2 eq 'primitive');
            $module = $3;
            $line =~ s/$1//;
            $line =~ s/^\s+|\s+$//g;
        }
    }

    if (!$module) {
        print STDERR "No module found in $file\n";
        goto getModuleHeader_close;
    }

    my $block = "";
    unless ($line =~ s/;.*//) {
        $block .= $line;
        while (defined($line = readline($fh)) && !($line =~ s/;.*//)) {
            chomp $line;
            $line =~ s/^\s+|\s+$//g;
            $block .= $line;
        }
    }

    $block .= $line;
    $block =~ s/\r?\n?//g;

    unless($block =~ /(?:#\((.*)\))?\((.*)\)/) {
        print STDERR "Failed to find param/port block\n";
        goto getModuleHeader_close;
    }

    my $params = $1 // "";
    my $ports = $2;

    close($fh);
    return ($module, $params, $ports, $isPrimitive);

getModuleHeader_close:
    close($fh);
}

sub parseDeclarations {
    my $portsStr = shift;
    my @ports = split(',', $portsStr);
    my @r = ();
    my $type2 = 'output';
    foreach my $p (@ports) {
        $p = trim($p);
        if ($p =~ /(?:(.*)\s+)?([a-zA-Z\$][\w\$]*)(?:\s*=\s*([^\s].*))?/) {
            my ($type, $identifier, $defaultValue) = ($1, $2, $3);
            $type = trim($type);
            $identifier = trim($identifier);
            $defaultValue = trim($defaultValue);

            if ($type) {
                $type2 = $type;
            }

            push(@r, {
                'type' => $type2,
                'identifier' => $identifier,
                'defaultValue' => $defaultValue
            });
        }
    }
    return @r;
}

while (my $file = shift) {
    my $testbenchFile = $file;
    $testbenchFile =~ s/\.v$/$testSuffix.v/;

    if (-f $testbenchFile) {
        print STDERR "$testbenchFile already exists, not generating\n";
        next;
    }

    my ($module, $params, $ports, $isPrimitive) = getModuleHeader($file) or next;
    print "Generating module $module$testSuffix -> $testbenchFile\n";

    my @paramsList = parseDeclarations($params);
    my @portsList = parseDeclarations($ports);

    my $fh2;
    unless (open($fh2, '>', $testbenchFile)) {
        print STDERR "Failed to open $testbenchFile: $!\n";
    }

    print $fh2 "module $module$testSuffix ();\n";
    print $fh2 "\n";
    if (scalar @paramsList) {
        foreach my $p (@paramsList) {
            print $fh2 $indent . $p->{'type'} . " " . $p->{'identifier'};
            if (defined($p->{'defaultValue'})) {
                print $fh2 " = " . $p->{'defaultValue'};
            }
            print $fh2 ";\n";
        }
        print $fh2 "\n";
    }
    foreach my $p (@portsList) {
        my $type = $p->{'type'};
        $type =~ s/input|inout/reg/;
        $type =~ s/output(?:\s+reg)?/wire/;

        print $fh2 $indent . $type . " " . $p->{'identifier'};
        if (defined($p->{'defaultValue'})) {
            print $fh2 " = " . $p->{'defaultValue'};
        }
        print $fh2 ";\n";
    }
    print $fh2 "\n";
    print $fh2 "$indent$module";

    if (scalar @paramsList) {
        print $fh2 " #(" . join(",", map { local $x = $_->{'identifier'}; ".$x($x)" } @paramsList) . ")";
    }

    print $fh2 " uut (";
    if ($isPrimitive) {
        print $fh2 join(", ", map { $_->{'identifier'} } @portsList);
    } else {
        print $fh2 "\n";
        print $fh2 join(",\n", map { local $x = $_->{'identifier'}; "$indent$indent.$x($x)" } @portsList) . "\n";
        print $fh2 "${indent}";
    }
    print $fh2 ");\n";
    print $fh2 "\n";
    print $fh2 "${indent}initial \$monitor();\n";
    print $fh2 "\n";
    print $fh2 "${indent}initial begin\n";
    print $fh2 "\n";
    print $fh2 "${indent}${indent}#2 \$finish;\n";
    print $fh2 "${indent}end\n";
    print $fh2 "\n";
    print $fh2 "endmodule\n";

    close($fh2);
}
