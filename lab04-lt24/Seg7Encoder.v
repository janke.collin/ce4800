module Seg7Encoder (
    output reg [7:0] seg7,
    input [3:0] dataIn
);

    always @(dataIn) begin
        case (dataIn)
            0: seg7 = 8'b00111111;
            1: seg7 = 8'b00000110;
            2: seg7 = 8'b01011011;
            3: seg7 = 8'b01001111;
            4: seg7 = 8'b01100110;
            5: seg7 = 8'b01101101;
            6: seg7 = 8'b01111101;
            7: seg7 = 8'b00100111;
            8: seg7 = 8'b01111111;
            9: seg7 = 8'b01101111;
           10: seg7 = 8'b01110111;
           11: seg7 = 8'b01111100;
           12: seg7 = 8'b01011000;
           13: seg7 = 8'b01011110;
           14: seg7 = 8'b01111001;
           15: seg7 = 8'b01110001;
        endcase
    end
endmodule
