module LT24_DRAWING  (
	input         CLK_50  , 
	input         RESET_N , 
	output        CLK_1M  ,
	output [15:0] LT24_D,
	output        LT24_RD_N,
	output        LT24_RESET_N,
	output        LT24_RS,
	output        LT24_WR_N ,
   output        LT24_CS_N	 , 
	output     reg DRAW_READY  ,
   input      [6:0]HEX0_D ,
   input      [6:0]HEX1_D ,
   input      [6:0]HEX2_D ,
   input      [6:0]HEX3_D ,
   input      [6:0]HEX4_D ,
   input      [6:0]HEX5_D ,		
   input      HEX0_DT ,
   input      HEX1_DT ,
   input      HEX2_DT ,
   input      HEX3_DT ,
   input      HEX4_DT ,
   input      HEX5_DT ,
	input      [9:0] LED, 
	input      [3:0] KEY, 
	input      [9:0] SW, 

	//-----------------------test 
	output reg [7:0]ST  , 
	output reg [7:0]COM_REG  , 
	output reg [15:0]BYTE , 
	output reg [31:0]CNT ,	
   output reg [15:0]X_ORG,
   output reg [15:0]Y_ORG,
   output reg  W_GO ,
   output      W_END ,
   output reg  W_GO_7 ,
   output      W_END_7,	
	output reg  W_GO_B ,
   output      W_END_B,		
	output reg  W_GO_S ,
   output      W_END_S,		
	output reg  W_GO_L ,
   output      W_END_L,		
	output reg  W_GO_K ,
   output      W_END_K,			
	output reg  W_GO_V ,
   output      W_END_V,			
	output reg  W_GO_A ,
   output      W_END_A,			

	output  [7:0]	W_ST,
	output  [7:0] 	W_CNT,	
   output  reg  [7:0]  IROM_ADDR ,
   output  reg         IROM_CK ,
   output  wire [15:0] IROM_DAT
	);
	
//--------RESET 	
assign LT24_RESET_N = RESET_N  ; 

//-----STM CLOCK 
CLOCKMEM u1(  .CLK(CLK_50 ) ,.CLK_FREQ(100/2) ,.CK_1HZ (CLK_1M) ) ;  
 
//----------------------------------- main c  -- -
reg       [31:0]	DELY;
//---DRAW NUMBER 
parameter   NUMBER_SW_LED    =4'd10  ; 
parameter   NUMBER_KEY       =4'd4; 
parameter   NUMBER_HEX       =4'd6 ; 
parameter   NUMBER_VECTOR    =4'd1 ; 
parameter   NUMBER_ADJ    =4'd2 ; 


//---7-SEG OFFSET 
wire      [15:0 ]  Y_OSET ; 
wire      [15:0 ]  X_OSET ; 
assign       X_OSET  = 170  ; 
assign       Y_OSET  = 10 + ( 320/NUMBER_HEX  ) *CNT ; 

//---SWITCH OFFSET 
wire      [15:0 ]  Y_OSETS ; 
wire      [15:0 ]  X_OSETS ; 
assign       X_OSETS  = 10+40  ; 
assign       Y_OSETS  = 2 + ( 320/NUMBER_SW_LED ) *CNT ; 


//---LED OFFSET 
wire      [15:0 ]  Y_OSETL ; 
wire      [15:0 ]  X_OSETL ; 
assign       X_OSETL  = 80+40  ; 
assign       Y_OSETL  = 2 + ( 32 ) *CNT ; 


//---KEY OFFSET 
wire      [15:0 ]  Y_OSETK ; 
wire      [15:0 ]  X_OSETK ; 
assign       X_OSETK  = 2 ; 
assign       Y_OSETK  = 50+ ( 320/ ( NUMBER_KEY+4) ) *CNT ; 


//---VECTOR OFFSET 
wire      [15:0 ]  Y_OSETV ; 
wire      [15:0 ]  X_OSETV ; 
assign       X_OSETV  = 2 ; 
assign       Y_OSETV  = 300; 


//---VECTOR OFFSET 
wire      [15:0 ]  Y_OSETA ; 
wire      [15:0 ]  X_OSETA ; 
assign       X_OSETA  = 2 +  240 *CNT ;  
assign       Y_OSETA  = 2 +  320 *CNT ; 



//------------------------------MAIN C
reg   [7:0]  rCSEG   ;
//jeff nigh
reg   [7:0]  rCSEG0   ;
reg   [7:0]  rCSEG1   ;
reg   [7:0]  rCSEG2   ;
reg   [7:0]  rCSEG3   ;
reg   [7:0]  rCSEG4   ;
reg   [7:0]  rCSEG5   ;
// end jeff nigh	 
 
reg   [9:0]  rLED   ; 
reg   [3:0]  rKEY;
reg  [9:0]  rSW   ;
//// start  jeff nigh
//reg      [6:0]HEX0_D ,
//reg      [6:0]HEX1_D ,
//reg      [6:0]HEX2_D ,
//reg      [6:0]HEX3_D ,
//reg      [6:0]HEX4_D ,
//reg      [6:0]HEX5_D ,
//reg      HEX0_DT ,
//reg      HEX1_DT ,
//reg      HEX2_DT ,
//reg      HEX3_DT ,
//reg      HEX4_DT ,
//reg      HEX5_DT ,
//// end jeff nigh changes	
 
always @( negedge RESET_N or posedge CLK_1M )  begin 
 if (!RESET_N ) begin 
    ST<= 1; 	 
	 IROM_ADDR <=0; 
	 IROM_CK <=0 ; 
	 CNT     <=0;
	 DRAW_READY <=0; 
	 W_GO    <=1;
	 W_GO_B  <=1; //BLANKING  BLOCK TRIGGER 
	 W_GO_7  <=1; //7-SEG BLOCK  TRIGGER 
	 W_GO_S  <=1; //SWITCH  BLOCK  TRIGGER 	 
	 W_GO_L  <=1; //LED BLOCK TRIGGER 
	 W_GO_K  <=1; //KEY BLOCK TRIGGER 	 
	 W_GO_V  <=1; //VECTOR BLOCK TRIGGER 	 	 
	 W_GO_A  <=1; //ADJ DOT  BLOCK TRIGGER
	 rCSEG  <= CSEG ;
// start jeff nigh
    rCSEG1 <= CSEG1;
    rCSEG2 <= CSEG2;
    rCSEG3 <= CSEG3;
    rCSEG4 <= CSEG4;
    rCSEG5 <= CSEG5;
// end jeff nigh	 
	 rLED   <= LED  ; 
	 rKEY   <= KEY  ; 
	 rSW    <= SW  ; 
    end
  else 
   begin  
    rCSEG <= CSEG ;
//jeff nigh
    rCSEG1 <= CSEG1;
    rCSEG2 <= CSEG2;
    rCSEG3 <= CSEG3;
    rCSEG4 <= CSEG4;
    rCSEG5 <= CSEG5;

// end jeff nigh	 
	 
    rLED  <= LED  ; 
    rKEY  <= KEY  ; 
    rSW   <= SW  ; 
	case ( ST )
	1: begin 
   ST<=2;
   IROM_CK <=1; 
	end	
	2: begin 
        IROM_CK <=0 ; 	 
        if ( W_END ) begin  W_GO  <=0; ST<=3 ; DELY<=0;  end
	end                // Write pointer
	3: begin 
    DELY  <=DELY +1;
    if ( DELY ==2 ) begin 
     W_GO  <=1;
     ST<=4 ; 
	 end
	end       
	4: begin 
   if (W_END ) begin  
	          ST<=5 ;
     			 IROM_ADDR <= IROM_ADDR+1 ; 
				 DELY<=0;  
   end				 
	end              
	5: begin 
	    if ( IROM_ADDR == 100) ST <= 40 ; 
		 else begin 
		        if     ((IROM_ADDR ==88) || (IROM_ADDR ==89))  begin 
							 if ( DELY < 20000  )   DELY <=DELY+1 ; 
							   else 
							    begin 
	            	          ST <= 2 ; 
				                IROM_CK <=1 ; 								  
								 end 
              end 
              else  begin 				  
		          ST <= 2 ; 
				    IROM_CK <=1 ; 
				  end 
		 	   end  
	   end	
	6: begin 
      IROM_CK <=0 ; 
	   ST <= 7 ;
		CNT <= 0 ;
		end		
	//----------------------------DRAW 7-SEG
	7: begin  
	      {  X_ORG , Y_ORG } <=  { X_OSET[15:0]  ,Y_OSET[15:0] }  ; 
         if ( W_END_7 ) begin   W_GO_7  <=0; ST<=8 ; DELY<=0;  end
	end                // Write pointer
	8: begin 
    DELY  <=DELY +1;
    if ( DELY ==2 ) begin 
     W_GO_7  <=1;
     ST<=9 ; 
	 end
	end       
	9: begin 
      if (W_END_7 )   begin 
		   ST<=10 ;
			CNT <=CNT+1 ; 
		end	
   end
	10: begin 
	     if  ( CNT ==NUMBER_HEX) ST<=11 ;
		   else ST<= 7  ;
   end
	11: begin 
				       if ( rSW   != SW)    begin ST<= 44  ; CNT <= 0 ; DRAW_READY<=0 ;end 
			     else if ( rLED  != LED)   begin ST<= 49  ; CNT <= 0 ; DRAW_READY<=0 ;end 
				  else if ( rKEY  != KEY)   begin ST<= 55  ; CNT <= 0 ; DRAW_READY<=0 ;end 
	           else if ( rCSEG != CSEG ) begin ST<= 7   ; CNT <= 0 ; DRAW_READY<=0 ;end 
				  // start jeff nigh
				  else if ( rCSEG1 != CSEG1 ) begin ST<= 7   ; CNT <= 0 ; DRAW_READY<=0 ;end
				  else if ( rCSEG2 != CSEG2 ) begin ST<= 7   ; CNT <= 0 ; DRAW_READY<=0 ;end
				  else if ( rCSEG3 != CSEG3 ) begin ST<= 7   ; CNT <= 0 ; DRAW_READY<=0 ;end
				  else if ( rCSEG4 != CSEG4 ) begin ST<= 7   ; CNT <= 0 ; DRAW_READY<=0 ;end
				  else if ( rCSEG5 != CSEG5 ) begin ST<= 7   ; CNT <= 0 ; DRAW_READY<=0 ;end
              // end jeff nigh
			     else  begin 
					ST<=11 ;
					DRAW_READY<=1 ; 
					end
   end
	//-------------------------------DRAW BALNK 
	40: begin 
	   ST <= 41  ;
		end		
	41: begin  
         if ( W_END_B ) begin   W_GO_B  <=0; ST<=42 ; DELY<=0;  end
	end                // Write pointer
	42: begin 
    DELY  <=DELY +1;
    if ( DELY ==2 ) begin 
     W_GO_B  <=1;
     ST<=43 ; 
	 end
	end       
	43: begin 
      if (W_END_B )   begin 
		   ST<=44 ;
		end	
   end
	//--------------------------------DRAW SWITCH	
	44: begin 
		    ST<= 45   ;
		    CNT <= 0 ;	 
   end

	45: begin  
	      {  X_ORG , Y_ORG } <=  { X_OSETS[15:0]  ,Y_OSETS[15:0] }  ; 
         if ( W_END_S ) begin   W_GO_S  <=0; ST<=46 ; DELY<=0;  end
	end                // Write pointer
	46: begin 
    DELY  <=DELY +1;
    if ( DELY ==2 ) begin 
     W_GO_S  <=1;
     ST<=47 ; 
	 end
	end       
	47: begin 
      if (W_END_S )   begin 
		   ST<=48 ;
			CNT <=CNT+1 ; 
		end	
   end
	48: begin 
	     if  ( CNT ==NUMBER_SW_LED)  begin ST<= 49 ; end
		   else ST<= 45 ;
   end

	//--------------------------------------DRAW LED ----- 
	49: begin 
		    ST<= 50   ;
		    CNT <= 0 ;	 
   end
	50: begin  
	      {  X_ORG , Y_ORG } <=  { X_OSETL[15:0]  ,Y_OSETL[15:0] }  ; 
         if ( W_END_L ) begin   W_GO_L  <=0; ST<=51 ; DELY<=0;  end
	end               
	51: begin 
    DELY  <=DELY +1;
    if ( DELY ==2 ) begin 
     W_GO_L  <=1;
     ST<=52 ; 
	 end
	end       
	52: begin 
      if (W_END_L )   begin 
		   ST<=53 ;
			CNT <=CNT+1 ; 
		end	
   end
	53: begin 
	     if  ( CNT == NUMBER_SW_LED) ST<= 54  ;
		   else ST<= 50 ;
   end
	54: begin 
		ST<=55 ;
   end
	//----------------------------------DRAW KEY ----- 
	55: begin 
		    ST  <= 56  ;
		    CNT <= 0 ;	 
   end
	56: begin  
	      {  X_ORG , Y_ORG } <=  { X_OSETK[15:0]  ,Y_OSETK[15:0] }  ; 
         if ( W_END_K ) begin   W_GO_K <=0; ST<=57 ; DELY<=0;  end
	end               
	57: begin 
    DELY  <=DELY +1;
    if ( DELY ==2 ) begin 
     W_GO_K  <=1;
     ST<=58 ; 
	 end
	end       
	58: begin 
      if (W_END_K )   begin 
		   ST<=59 ;
			CNT <=CNT+1 ; 
		end	
   end
	59: begin 
	     if  ( CNT ==NUMBER_KEY) ST<= 60  ;
		   else ST<= 56 ;
   end
	//----------------------------------DRAW VECTOR ----- 
	60: begin 
		    ST  <= 61 ;
		    CNT <= 0 ;	 
   end
	61: begin  
	      {  X_ORG , Y_ORG } <=  { X_OSETV[15:0]  ,Y_OSETV[15:0] }  ; 
         if ( W_END_V ) begin   W_GO_V <=0; ST<=62 ; DELY<=0;  end
	end               
	62: begin 
    DELY  <=DELY +1;
    if ( DELY ==2 ) begin 
     W_GO_V  <=1;
     ST<=63 ; 
	 end
	end       
	63: begin 
      if (W_END_V )   begin 
		   ST<=64 ;
			CNT <=CNT+1 ; 
		end	
   end
	64: begin 
	     if  ( CNT == NUMBER_VECTOR) ST<= 65  ;
		   else ST<= 61 ;
   end
	//----------------------------------DRAW ADJ ----- 
	65: begin 
		    ST  <= 66 ;
		    CNT <= 0 ;	 
   end
	66: begin  
	      {  X_ORG , Y_ORG } <=  { X_OSETA[15:0]  ,Y_OSETA[15:0] }  ; 
         if ( W_END_A ) begin   W_GO_A <=0; ST<=67 ; DELY<=0;  end
	end               
	67: begin 
    DELY  <=DELY +1;
    if ( DELY ==2 ) begin 
     W_GO_A  <=1;
     ST<=68 ; 
	 end
	end       
	68: begin 
      if (W_END_A )   begin 
		   ST<=69 ;
			CNT <=CNT+1 ; 
		end	
   end
	69: begin 
	     if  ( CNT == NUMBER_ADJ) ST<= 70  ;
		   else ST<= 66 ;
   end
	

	
	70: begin 
		ST<=6 ;
   end
endcase
end
end


//-- INIT ROM --- 
INI_ROM  in_r(
	.address (IROM_ADDR) ,
	.clock   (IROM_CK  ) ,
	.q       (IROM_DAT ) 
	);

//--- LT24 "INITIAL" ----
 WRITE_INIT  in1(
 	.GO       ( W_GO ) ,
	.END_OK   ( W_END) ,	
   .RESET_N  ( RESET_N ),
	.PT_CK    ( CLK_1M),
	.DATA16   ( IROM_DAT[15:0] ) ,
   //------ INTER FACE	
	.LT24_D   (LT24_D_I   ),
	.LT24_RD_N(LT24_RD_N_I) ,
	.LT24_RS  (LT24_RS_I  ),
	.LT24_WR_N(LT24_WR_N_I) ,
   .LT24_CS_N(LT24_CS_N_I), 
   //TEST 
	.ST  (W_ST ),
	.CNT (W_CNT),
	.BYTE()  
);
wire [15:0] LT24_D_I    ; 
wire LT24_RD_N_I ;
wire LT24_RS_I  	;
wire LT24_WR_N_I;
wire LT24_CS_N_I;


//--- SEG  ----- 
wire     [7:0]  CSEG   ; 
assign    CSEG   = (
   (CNT ==0  )?{ HEX5_DT  , HEX5_D[6:0]   }  :  (  
   (CNT ==1  )?{ HEX4_DT  , HEX4_D[6:0]   }  :  (  
   (CNT ==2  )?{ HEX3_DT  , HEX3_D[6:0]   }  :  (  
   (CNT ==3  )?{ HEX2_DT  , HEX2_D[6:0]   }  :  (  
   (CNT ==4  )?{ HEX1_DT  , HEX1_D[6:0]   }  :  (  
   (CNT ==5  )?{ HEX0_DT  , HEX0_D[6:0]   }  :  { HEX0_DT  , HEX0_D[6:0]   }
	)))))) ;

// start jeff nigh
wire     [7:0]  CSEG5   ; 
wire     [7:0]  CSEG4   ; 
wire     [7:0]  CSEG3   ; 
wire     [7:0]  CSEG2   ; 
wire     [7:0]  CSEG1   ; 

assign CSEG5 = { HEX5_DT  , HEX5_D[6:0]   };
assign CSEG4 = { HEX4_DT  , HEX4_D[6:0]   };
assign CSEG3 = { HEX3_DT  , HEX3_D[6:0]   };
assign CSEG2 = { HEX2_DT  , HEX2_D[6:0]   };
assign CSEG1 = { HEX1_DT  , HEX1_D[6:0]   };
// end jeff nigh
	
	
//---	
wire [7:0] CSG77 ; 
assign CSG77[7:0] 	  = CSEG[7:0] ;

//---DRAW "7-SEG " -- 
DRAW_7SEG  seg (
 	.GO       ( W_GO_7 ) ,
	.END_OK   ( W_END_7) ,	
   .RESET_N  ( RESET_N ),
	.PT_CK    ( CLK_1M),
	.HEX_D    (  CSG77 ) , 
	.X_ORG    (  X_ORG),
	.Y_ORG    (  Y_ORG),
	.DATA16   ( IROM_DAT[15:0] ) ,
   //------ INTER FACE	
	.LT24_D   (LT24_D_7   ),
	.LT24_RD_N(LT24_RD_N_7) ,
	.LT24_RS  (LT24_RS_7  ),
	.LT24_WR_N(LT24_WR_N_7) ,
   .LT24_CS_N(LT24_CS_N_7), 
   //TEST 
	.ST  ( ),
	.CNT (),
	.BYTE()  
);
wire [15:0] LT24_D_7    ; 
wire LT24_RD_N_7 ;
wire LT24_RS_7  	;
wire LT24_WR_N_7;
wire LT24_CS_N_7;

//---DRAW "ALL BLANK "-----
DRAW_ALL_BLANK  bk (
 	.GO       ( W_GO_B ) ,
	.END_OK   ( W_END_B) ,	
	
   .RESET_N  ( RESET_N ),
	.PT_CK    ( CLK_1M),
	
	.X_ORG    (  0),
	.Y_ORG    (  0),
	
	.R  (32),  
   .G  (32),  
	.B  (32),  
   //------ INTER FACE	
	.LT24_D   ( LT24_D_B   ),
	.LT24_RD_N( LT24_RD_N_B) ,
	.LT24_RS  ( LT24_RS_B  ),
	.LT24_WR_N( LT24_WR_N_B) ,
   .LT24_CS_N( LT24_CS_N_B), 
   //TEST 
	.ST  ( ),
	.CNT (),
	.BYTE()  
);
wire [15:0] LT24_D_B    ; 
wire LT24_RD_N_B ;
wire LT24_RS_B  	;
wire LT24_WR_N_B;
wire LT24_CS_N_B;


//---DRAW "SWITCH" -- 
wire    SW_ON_OFF;
assign  SW_ON_OFF = (
 (CNT==0 )  && ( SW[9] ==1 )?1 : ( 
 (CNT==1 )  && ( SW[8] ==1 )?1 : ( 
 (CNT==2 )  && ( SW[7] ==1 )?1 : ( 
 (CNT==3 )  && ( SW[6] ==1 )?1 : ( 
 (CNT==4 )  && ( SW[5] ==1 )?1 : ( 
 (CNT==5 )  && ( SW[4] ==1 )?1 : ( 
 (CNT==6 )  && ( SW[3] ==1 )?1 : ( 
 (CNT==7 )  && ( SW[2] ==1 )?1 : ( 
 (CNT==8 )  && ( SW[1] ==1 )?1 : ( 
 (CNT==9 )  && ( SW[0] ==1 )?1 : 0
 )))))))))); 

DRAW_SWITCH  SQ (
 	.GO       ( W_GO_S ) ,
	.END_OK   ( W_END_S) ,	
   .RESET_N  ( RESET_N ),
	.PT_CK    ( CLK_1M),
   .SW( SW_ON_OFF  ) , 
	.X_ORG    ( X_ORG),
	.Y_ORG    ( Y_ORG),
   //-- INTERFCAE 
	.LT24_D   (LT24_D_S   ),
	.LT24_RD_N(LT24_RD_N_S) ,
	.LT24_RS  (LT24_RS_S  ),
	.LT24_WR_N(LT24_WR_N_S) ,
   .LT24_CS_N(LT24_CS_N_S), 
   //TEST 
	.ST  ( ),
	.CNT () 
);
wire [15:0] LT24_D_S    ; 
wire LT24_RD_N_S ;
wire LT24_RS_S  	;
wire LT24_WR_N_S;
wire LT24_CS_N_S;

//-----NUBER 
wire [7:0] LED_SEG  ; 
assign LED_SEG[6:0] 
  = 7'h7f ^ ( 
( NUM_L[3:0] == 1 ) ? 7'b1111001:(  
( NUM_L[3:0] == 2 ) ? 7'b0100100:  (  
( NUM_L[3:0] == 3 ) ? 7'b0110000:  (  
( NUM_L[3:0] == 4 ) ? 7'b0011001:  (  
( NUM_L[3:0] == 5 ) ? 7'b0010010:  (  
( NUM_L[3:0] == 6 ) ? 7'b0000010:  (  
( NUM_L[3:0] == 7 ) ? 7'b1111000:  (  
( NUM_L[3:0] == 8 ) ? 7'b0000000:  (  
( NUM_L[3:0] == 9 ) ? 7'b0011000:  (  
( NUM_L[3:0] == 0 ) ?7'b1000000:  7'b1000000
))))))))));

wire [3:0 ] NUM_L ; 
assign NUM_L = 9- CNT  ; 


//---DRAW "LED" -- 
wire    LED_ON_OFF;
assign  LED_ON_OFF = (
 (CNT==0 )  && ( LED[9] ==1 )?1 : ( 
 (CNT==1 )  && ( LED[8] ==1 )?1 : ( 
 (CNT==2 )  && ( LED[7] ==1 )?1 : ( 
 (CNT==3 )  && ( LED[6] ==1 )?1 : ( 
 (CNT==4 )  && ( LED[5] ==1 )?1 : ( 
 (CNT==5 )  && ( LED[4] ==1 )?1 : ( 
 (CNT==6 )  && ( LED[3] ==1 )?1 : ( 
 (CNT==7 )  && ( LED[2] ==1 )?1 : ( 
 (CNT==8 )  && ( LED[1] ==1 )?1 : ( 
 (CNT==9 )  && ( LED[0] ==1 )?1 : 0
)))))))))); 


DRAW_LED  SLED (
 	.GO       ( W_GO_L ) ,
	.END_OK   ( W_END_L ) ,	
   .RESET_N  ( RESET_N ),
	.PT_CK    ( CLK_1M),
	.LED      ( LED_ON_OFF ) , 	
	.X_ORG    ( X_ORG),
	.Y_ORG    ( Y_ORG),
	.HEX_D     (LED_SEG ), 
   //--INTER FACE	
	.LT24_D   ( LT24_D_L   ),
	.LT24_RD_N( LT24_RD_N_L) ,
	.LT24_RS  ( LT24_RS_L  ),
	.LT24_WR_N( LT24_WR_N_L) ,
   .LT24_CS_N( LT24_CS_N_L), 
   //--TEST 
	.ST  ( ),
	.CNT () 
);
wire [15:0] LT24_D_L    ; 
wire LT24_RD_N_L ;
wire LT24_RS_L  	;
wire LT24_WR_N_L;
wire LT24_CS_N_L;

//---DRAW "KEY"--
wire    KET_ON_OFF;
assign  KEY_ON_OFF = (
 (CNT==0 )  && ( KEY[3] ==1 )?1 : ( 
 (CNT==1 )  && ( KEY[2] ==1 )?1 : ( 
 (CNT==2 )  && ( KEY[1] ==1 )?1 : ( 
 (CNT==3 )  && ( KEY[0] ==1 )?1 : 0
)))); 


DRAW_KEY   SKEY (
 	.GO       ( W_GO_K ) ,
	.END_OK   ( W_END_K) ,	
	
   .RESET_N  ( RESET_N ),
	.PT_CK    ( CLK_1M),
   .KEY      ( KEY_ON_OFF ) , 
	
	.X_ORG    (  X_ORG),
	.Y_ORG    (  Y_ORG),
	

   //------INTERFACE---  
	.LT24_D   (LT24_D_K   ),
	.LT24_RD_N(LT24_RD_N_K) ,
	.LT24_RS  (LT24_RS_K  ),
	.LT24_WR_N(LT24_WR_N_K) ,
   .LT24_CS_N(LT24_CS_N_K), 
   //TEST 
	.ST  ( ),
	.CNT () 
);
wire [15:0] LT24_D_K    ; 
wire LT24_RD_N_K ;
wire LT24_RS_K  	;
wire LT24_WR_N_K;
wire LT24_CS_N_K;

//---DRAW "VECTOR"--

DRAW_VECTOR    VECT (
 	.GO       ( W_GO_V ) ,
	.END_OK   ( W_END_V) ,	
   .RESET_N  ( RESET_N ),
	.PT_CK    ( CLK_1M),
   //.KEY      ( KEY_ON_OFF ) ,
	.X_ORG    (  X_ORG),
	.Y_ORG    (  Y_ORG),
   //------INTERFACE---  
	.LT24_D   (LT24_D_V   ),
	.LT24_RD_N(LT24_RD_N_V) ,
	.LT24_RS  (LT24_RS_V  ),
	.LT24_WR_N(LT24_WR_N_V) ,
   .LT24_CS_N(LT24_CS_N_V), 
   //TEST 
	.ST  (),
	.CNT () 
);
wire [15:0] LT24_D_V; 
wire LT24_RD_N_V ;
wire LT24_RS_V  	;
wire LT24_WR_N_V;
wire LT24_CS_N_V;



//---  DRAW_ADJ DOT 
DRAW_ADJ   SADJ (
 	.GO       ( W_GO_A ) ,
	.END_OK   ( W_END_A) ,	
	
   .RESET_N  ( RESET_N ),
	.PT_CK    ( CLK_1M),
	.X_ORG    (  X_ORG),
	.Y_ORG    (  Y_ORG),
	
   //------ INTER FACE	
	.LT24_D   (LT24_D_A   ),
	.LT24_RD_N(LT24_RD_N_A) ,
	.LT24_RS  (LT24_RS_A  ),
	.LT24_WR_N(LT24_WR_N_A) ,
   .LT24_CS_N(LT24_CS_N_A), 
   //TEST 
	.ST  ( ),
	.CNT () 
);
wire [15:0] LT24_D_A    ; 
wire LT24_RD_N_A ;
wire LT24_RS_A  	;
wire LT24_WR_N_A;
wire LT24_CS_N_A;




//--- SUM LCD TIMING
assign LT24_D     =LT24_D_I    & LT24_D_7    & LT24_D_B    & LT24_D_S    & LT24_D_L     & LT24_D_K    & LT24_D_V    & LT24_D_A    ;
assign LT24_RD_N  =LT24_RD_N_I & LT24_RD_N_7 & LT24_RD_N_B & LT24_RD_N_S & LT24_RD_N_L  & LT24_RD_N_K & LT24_RD_N_V & LT24_RD_N_A ;
assign LT24_RS    =LT24_RS_I   & LT24_RS_7   & LT24_RS_B   & LT24_RS_S   & LT24_RS_L    & LT24_RS_K   & LT24_RS_V   & LT24_RS_A   ;
assign LT24_WR_N  =LT24_WR_N_I & LT24_WR_N_7 & LT24_WR_N_B & LT24_WR_N_S & LT24_WR_N_L  & LT24_WR_N_K & LT24_WR_N_V & LT24_WR_N_A ;
assign LT24_CS_N  =LT24_CS_N_I & LT24_CS_N_7 & LT24_CS_N_B & LT24_CS_N_S & LT24_CS_N_L  & LT24_CS_N_K & LT24_CS_N_V & LT24_CS_N_A ;


endmodule 
