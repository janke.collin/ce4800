module TOUCH2SW ( 
               input RESET_N , 
               input          CLK ,
					input          DRAW_READY ,
               input          TOUCH_IRQ   ,
					input   [11:0] X_COORD     ,
					input   [11:0] Y_COORD ,
					output  reg [9:0] SW          

) ; 

reg    [11:0] rX_COORD;
reg    [11:0] rY_COORD;
reg    rTOUCH_IRQ  ; 
//SW0_0
parameter X0_0_1 = 12'hd99;
parameter Y0_0_1 = 12'h444;

parameter X0_0_2 = 12'he21;
parameter Y0_0_2 = 12'h59e;
//SW0_1
parameter X0_1_1 = 12'hda2;
parameter Y0_1_1 = 12'h5d0;

parameter X0_1_2 = 12'he1d;
parameter Y0_1_2 = 12'h70f;

//SW1_0
parameter X1_0_1 = 12'hc45;
parameter Y1_0_1 = 12'h46d;

parameter X1_0_2 = 12'hcb3;
parameter Y1_0_2 = 12'h5ad;

//SW1_1                   
parameter X1_1_1 = 12'hc4b;
parameter Y1_1_1 = 12'h5cd;

parameter X1_1_2 = 12'hcca;
parameter Y1_1_2 = 12'h720;


//SW2_0
parameter X2_0_1 = 12'hadb;
parameter Y2_0_1 = 12'h46c;

parameter X2_0_2 = 12'hb35;
parameter Y2_0_2 = 12'h562;

//SW2_1                   
parameter X2_1_1 = 12'hacf;
parameter Y2_1_1 = 12'h5ab;

parameter X2_1_2 = 12'hb4e;
parameter Y2_1_2 = 12'h6f6;

//SW3_0
parameter X3_0_1 = 12'h96c;
parameter Y3_0_1 = 12'h46f;

parameter X3_0_2 = 12'h9e5;
parameter Y3_0_2 = 12'h578;
//SW3_1                   
parameter X3_1_1 = 12'h976;
parameter Y3_1_1 = 12'h5a9;

parameter X3_1_2 = 12'h9e8;
parameter Y3_1_2 = 12'h6eb;

//SW4_0
parameter X4_0_1 = 12'h815;
parameter Y4_0_1 = 12'h455;

parameter X4_0_2 = 12'h888;
parameter Y4_0_2 = 12'h56f;
//SW4_1                   
parameter X4_1_1 = 12'h818;
parameter Y4_1_1 = 12'h5a5;

parameter X4_1_2 = 12'h884;
parameter Y4_1_2 = 12'h6fe;



//SW5_0
parameter X5_0_1 = 12'h6bd;
parameter Y5_0_1 = 12'h44f;

parameter X5_0_2 = 12'h723;
parameter Y5_0_2 = 12'h595;
//SW5_1                   
parameter X5_1_1 = 12'h6b2;
parameter Y5_1_1 = 12'h5bf;

parameter X5_1_2 = 12'h733;
parameter Y5_1_2 = 12'h6ff;
                          
//SW6_0                   
parameter X6_0_1 = 12'h54b;
parameter Y6_0_1 = 12'h47a;

parameter X6_0_2 = 12'h5bb;
parameter Y6_0_2 = 12'h564;
//SW6_1                   
parameter X6_1_1 = 12'h54a;
parameter Y6_1_1 = 12'h5c5;

parameter X6_1_2 = 12'h5c3;
parameter Y6_1_2 = 12'h71a;


//SW7_0
parameter X7_0_1 = 12'h3e3;
parameter Y7_0_1 = 12'h470;

parameter X7_0_2 = 12'h454;
parameter Y7_0_2 = 12'h580;
//SW7_1                   
parameter X7_1_1 = 12'h3e8;
parameter Y7_1_1 = 12'h5b6;

parameter X7_1_2 = 12'h455;
parameter Y7_1_2 = 12'h6fd;



//SW8_0
parameter X8_0_1 = 12'h284;
parameter Y8_0_1 = 12'h46a;

parameter X8_0_2 = 12'h2e3;
parameter Y8_0_2 = 12'h592;
//SW8_1                   
parameter X8_1_1 = 12'h282;
parameter Y8_1_1 = 12'h5cb;

parameter X8_1_2 = 12'h2f9;
parameter Y8_1_2 = 12'h70b;


//SW9_0
parameter X9_0_1 = 12'h12b;
parameter Y9_0_1 = 12'h479;

parameter X9_0_2 = 12'h19f;
parameter Y9_0_2 = 12'h595;
//SW9_1
parameter X9_1_1 = 12'h120;
parameter Y9_1_1 = 12'h5c5;

parameter X9_1_2 = 12'h1a3;
parameter Y9_1_2 = 12'h6e6;








always @( negedge RESET_N  or negedge CLK )  begin
if ( !RESET_N  )    begin 
 rX_COORD <= X_COORD ;
 rY_COORD <= Y_COORD ;
 SW  <= 10'b1111111111 ; 
end 

else begin 
rX_COORD <= X_COORD ;
rY_COORD <= Y_COORD ;
if  ( ( DRAW_READY==1) && ( (rX_COORD != X_COORD) ||  (rY_COORD != Y_COORD))  )  begin 
//--sw0
       if (( X_COORD > X0_0_1) && ( X_COORD<X0_0_2) && ( Y_COORD >Y0_0_1) && ( Y_COORD < Y0_0_2) )  SW[0] <=0;  //SW[0] <=0
  else if (( X_COORD > X0_1_1) && ( X_COORD<X0_1_2) && ( Y_COORD >Y0_1_1) && ( Y_COORD < Y0_1_2) )  SW[0] <=1;  //SW[0] <=1  
                                                                                                                     
//--sw1                                                                                                              
       if (( X_COORD > X1_0_1) && ( X_COORD<X1_0_2) && ( Y_COORD >Y1_0_1) && ( Y_COORD < Y1_0_2) )  SW[1] <=0;  //SW[1] <=0
  else if (( X_COORD > X1_1_1) && ( X_COORD<X1_1_2) && ( Y_COORD >Y1_1_1) && ( Y_COORD < Y1_1_2) )  SW[1] <=1;  //SW[1] <=1  
                                                                                                                     
//--sw2                                                                                                              
       if (( X_COORD > X2_0_1) && ( X_COORD<X2_0_2) && ( Y_COORD >Y2_0_1) && ( Y_COORD < Y2_0_2) )  SW[2] <=0;  //SW[2] <=0
  else if (( X_COORD > X2_1_1) && ( X_COORD<X2_1_2) && ( Y_COORD >Y2_1_1) && ( Y_COORD < Y2_1_2) )  SW[2] <=1;  //SW[2] <=1  
                                                                                                                     
//--sw3                                                                                                              
       if (( X_COORD > X3_0_1) && ( X_COORD<X3_0_2) && ( Y_COORD >Y3_0_1) && ( Y_COORD < Y3_0_2) )  SW[3] <=0;  //SW[3] <=0
  else if (( X_COORD > X3_1_1) && ( X_COORD<X3_1_2) && ( Y_COORD >Y3_1_1) && ( Y_COORD < Y3_1_2) )  SW[3] <=1;  //SW[3] <=1  
                                                                                                                     
//--sw4                                                                                                              
       if (( X_COORD > X4_0_1) && ( X_COORD<X4_0_2) && ( Y_COORD >Y4_0_1) && ( Y_COORD < Y4_0_2) )  SW[4] <=0;  //SW[4] <=0
  else if (( X_COORD > X4_1_1) && ( X_COORD<X4_1_2) && ( Y_COORD >Y4_1_1) && ( Y_COORD < Y4_1_2) )  SW[4] <=1;  //SW[4] <=1  
                                                                                                                     
//--sw5                                                                                                              
       if (( X_COORD > X5_0_1) && ( X_COORD<X5_0_2) && ( Y_COORD >Y5_0_1) && ( Y_COORD < Y5_0_2) )  SW[5] <=0;  //SW[5] <=0
  else if (( X_COORD > X5_1_1) && ( X_COORD<X5_1_2) && ( Y_COORD >Y5_1_1) && ( Y_COORD < Y5_1_2) )  SW[5] <=1;  //SW[5] <=1  
                                                                                                                     
//--sw6                                                                                                              
       if (( X_COORD > X6_0_1) && ( X_COORD<X6_0_2) && ( Y_COORD >Y6_0_1) && ( Y_COORD < Y6_0_2) )  SW[6] <=0;  //SW[6] <=0
  else if (( X_COORD > X6_1_1) && ( X_COORD<X6_1_2) && ( Y_COORD >Y6_1_1) && ( Y_COORD < Y6_1_2) )  SW[6] <=1;  //SW[6] <=1  
                                                                                                                     
//--sw7                                                                                                              
       if (( X_COORD > X7_0_1) && ( X_COORD<X7_0_2) && ( Y_COORD >Y7_0_1) && ( Y_COORD < Y7_0_2) )  SW[7] <=0;  //SW[7] <=0
  else if (( X_COORD > X7_1_1) && ( X_COORD<X7_1_2) && ( Y_COORD >Y7_1_1) && ( Y_COORD < Y7_1_2) )  SW[7] <=1;  //SW[7] <=1  
                                                                                                                     
//--sw8                                                                                                              
       if (( X_COORD > X8_0_1) && ( X_COORD<X8_0_2) && ( Y_COORD >Y8_0_1) && ( Y_COORD < Y8_0_2) )  SW[8] <=0;  //SW[8] <=0
  else if (( X_COORD > X8_1_1) && ( X_COORD<X8_1_2) && ( Y_COORD >Y8_1_1) && ( Y_COORD < Y8_1_2) )  SW[8] <=1;  //SW[8] <=1  
                                                                                                                     
//--sw9                                                                                                              
       if (( X_COORD > X9_0_1) && ( X_COORD<X9_0_2) && ( Y_COORD >Y9_0_1) && ( Y_COORD < Y9_0_2) )  SW[9] <=0;  //SW[9] <=0
  else if (( X_COORD > X9_1_1) && ( X_COORD<X9_1_2) && ( Y_COORD >Y9_1_1) && ( Y_COORD < Y9_1_2) )  SW[9] <=1;  //SW[9] <=1  
  
  end
 end  
end



endmodule 
