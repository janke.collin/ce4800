module TOUCH2KEY( 
               input RESET_N , 
               input   CLK         ,
				   input  DRAW_READY  , 
               input   TOUCH_IRQ   ,
					input   [11:0] X_COORD     ,
					input   [11:0] Y_COORD ,
					output  reg [3:0] KEY 				

) ; 

reg    [11:0] rX_COORD;
reg    [11:0] rY_COORD;
reg    rTOUCH_IRQ  ; 
//KEY3
parameter X3_1 = 12'h2fb;
parameter Y3_1 = 12'h182;

parameter X3_2 = 12'h3d3;
parameter Y3_2 = 12'h2d2;

//KEY2
parameter X2_1 = 12'h4ab;
parameter Y2_1 = 12'h183;

parameter X2_2 = 12'h5a3;
parameter Y2_2 = 12'h2e1;


//KEY1
parameter X1_1 = 12'h683;
parameter Y1_1 = 12'h16f;

parameter X1_2 = 12'h76f;
parameter Y1_2 = 12'h349;


//KEY0
parameter X0_1 = 12'h83d;
parameter Y0_1 = 12'h167;

parameter X0_2 = 12'h918;
parameter Y0_2 = 12'h2d7;


wire HZ_1  ; 
CLOCKMEM u2(  .CLK(CLK ) ,.CLK_FREQ(25000000/8) ,.CK_1HZ (HZ_1) ) ; 

reg[31:0] TIMER  ; 
reg TOUT ; 
always @( negedge TOUCH_IRQ or posedge  HZ_1 ) 
if  ( !TOUCH_IRQ )  begin TIMER <=0; TOUT  <=0; end 
   else if (  TIMER  > 1)   TOUT  <=1;
	  else TIMER <= TIMER+1; 

always @( negedge RESET_N  or negedge CLK )  begin
if ( !RESET_N  )
begin 
rX_COORD <= X_COORD ;
rY_COORD <= Y_COORD ;
KEY <= 4'b1111 ; 

end
else begin 
rX_COORD <= X_COORD ;
rY_COORD <= Y_COORD ;
if  ( DRAW_READY==1 ) begin 
   if  ( TOUT==1 ) begin 
	    KEY <= 4'hf ; 
	end 
	else begin 
  if  ( (rX_COORD != X_COORD) ||  (rY_COORD != Y_COORD))  begin 
 //--KEY 3
  if (( X_COORD > X3_1) && ( X_COORD<X3_2) && ( Y_COORD >Y3_1) && ( Y_COORD < Y3_2) ) //KEY3
		       KEY[3] <=0 ; 
	    else  KEY[3] <=1 ; 
 //--KEY 2
  if (( X_COORD > X2_1) && ( X_COORD<X2_2) && ( Y_COORD >Y2_1) && ( Y_COORD < Y2_2) ) //KEY3
		       KEY[2] <=0 ; 
	    else  KEY[2] <=1 ; 
 //--KEY 1
  if (( X_COORD > X1_1) && ( X_COORD<X1_2) && ( Y_COORD >Y1_1) && ( Y_COORD < Y1_2) ) //KEY3
		       KEY[1] <=0 ; 
	    else  KEY[1] <=1 ; 
 //--KEY 0
  if (( X_COORD > X0_1) && ( X_COORD<X0_2) && ( Y_COORD >Y0_1) && ( Y_COORD < Y0_2) ) //KEY3
		       KEY[0] <=0 ; 
	    else  KEY[0] <=1 ; 
  end
 end
 end
 end
 end


endmodule 
