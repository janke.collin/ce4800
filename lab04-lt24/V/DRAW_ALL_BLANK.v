module  DRAW_ALL_BLANK (
   input  				RESET_N ,
	input      			PT_CK,
	input      			GO,
   input             [4:0] R ,  
   input             [4:0] G ,  
	input             [4:0] B ,  
   input             [15:0]X_ORG,
   input             [15:0]Y_ORG,
	
	input      [15:0]	DATA16,	
	input      [7:0]	COM,	

	input      [7:0]	HEX_D,	

	
   input      [15:0]	BYTE , 
	
	output reg    [15:0]		LT24_D,
	output reg    LT24_RD_N,
	output        LT24_RESET_N,
	output reg    LT24_RS,
	output reg    LT24_WR_N ,
   output reg    LT24_CS_N	 , 
	
	output reg 			END_OK,
	
	//--for test 
	output reg [7:0]	ST ,
	output reg [31:0] 	CNT 
	
);

//==COLOR==
parameter BLACK  	   = 16'h0000; 
parameter BLUE		   = 16'h001F;
parameter RED		   = 16'hF800;
parameter GREEN		= 16'h07E0;
parameter CYAN		   = 16'h07FF;
parameter MAGENTA	   = 16'hF81F;
parameter YELLOW	   = 16'hFFE0;
parameter WHITE		= 16'hFFFF;


wire [15:0 ] R5_G6_B5 ;

reg   [7:0]DELY ;

wire  [15:0]Y_END ; 

wire  [15:0]X_END ; 

assign X_END = X_ORG + L-1  ; 

assign Y_END = Y_ORG + W-1 ; 

wire [15:0] L;
wire [15:0] W;

assign L =240 ;
assign W =320 ;

assign R5_G6_B5 = { R[4:0] , { G[4:0] , 1'b0 }  , B[4:0] }  ;  // R 0~32 , G 0~64  , B0~32
 //------------------------------------------------------------------------------------------
always @( negedge RESET_N or posedge  PT_CK )begin
if ( !RESET_N ) ST <=0;
else  begin 
case (ST)
 0: begin  //start 		      
	 CNT    <=0;
	 END_OK <=1;
    LT24_RD_N    <=1; 
	 LT24_CS_N    <=1; 	 
	 LT24_RS      <=1; 	 
	 LT24_WR_N    <=1; 
    LT24_D[15:0] <=16'hffff; 	
	 CNT <=0; 
	 if (GO) ST  <=30 ; 
    end	 
	  1: begin
	       ST<=2 ; 		  		  
			      if  ( CNT ==0 ) { LT24_D[7:0] , LT24_RS } <= {8'h2A       ,1'b0 } ;
			 else if  ( CNT ==1 ) { LT24_D[7:0] , LT24_RS } <= {X_ORG[15:8] ,1'b1 } ; // stat
			 else if  ( CNT ==2 ) { LT24_D[7:0] , LT24_RS } <= {X_ORG[7:0]  ,1'b1 } ;
			 else if  ( CNT ==3 ) { LT24_D[7:0] , LT24_RS } <= {X_END[15:8] ,1'b1 } ;  //end
			 else if  ( CNT ==4 ) { LT24_D[7:0] , LT24_RS } <= {X_END[7:0]  ,1'b1 } ;
			 else if  ( CNT ==5 ) { LT24_D[7:0] , LT24_RS } <= {8'h2B ,1'b0 } ;
			 else if  ( CNT ==6 ) { LT24_D[7:0] , LT24_RS } <= {Y_ORG[15:8] ,1'b1 } ;  //strt
			 else if  ( CNT ==7 ) { LT24_D[7:0] , LT24_RS } <= {Y_ORG[7:0 ] ,1'b1 } ;
			 else if  ( CNT ==8 ) { LT24_D[7:0] , LT24_RS } <= {Y_END[15:8] ,1'b1 } ;  //end
			 else if  ( CNT ==9 ) { LT24_D[7:0] , LT24_RS } <= {Y_END[7:0]  ,1'b1 } ;
			 else if  ( CNT ==10 ) { LT24_D[7:0] , LT24_RS} <= {8'h2c ,1'b0 } ;
			 
		  end 
	  2: begin
	     ST<=5 ;
	     LT24_CS_N <=0; 	 
		  end	 		  
	  5: begin
	     ST<=6 ; 
	     LT24_WR_N   <=0; 		  
		  CNT <=CNT+1 ;  
	  end
	  6: begin	     
	     LT24_WR_N <=1; 		 
		  ST<=7 ; 
	  end
	  7:begin	     
	     LT24_CS_N <=1; 	 
		  if ( CNT ==11 )  begin ST <= 8 ; CNT <=0 ; end
		   else ST <= 1 ; 
	   end
//-----DRAWN  DATA 	  
	  8:begin	
	   LT24_CS_N <=1;	
            {LT24_D[15:0] ,   LT24_RS  }  <=  { BLACK ,1'b1  }  ; 			  
			   ST<=9 ;
	    end
	  9:begin
	    LT24_CS_N <=0;	
	    ST<=10 ;	 
		end 
	  10:begin
	    LT24_WR_N <=0;	
		 CNT <=CNT+1;
	    ST<=11 ;	 
		end 
	  11:begin	  
	     LT24_WR_N <=1 ; 	 
		  if ( CNT ==  L*W )  begin ST <= 20 ; CNT <=0 ; end
		   else ST <= 8 ; 
	   end  
	  
	  
	  
	  
	  20: begin
	     ST<=30 ; 
        LT24_RD_N    <=1; 
	     LT24_CS_N    <=1; 	 
	     LT24_RS      <=1; 	 
	     LT24_WR_N    <=1; 
        LT24_D[15:0] <=16'hffff; 	 
	     END_OK <=1;	 
	     end
		  
//--END 		  
		30: begin
            if (!GO) ST  <=31;
          end
		31: begin  //
		      END_OK<=0;
				CNT   <=0; 
				ST    <=1;	
			end
	  endcase 
 
 end
 end
 /*
 //--ROM 7SEG 
 reg  [ 11:0 ] SEG7_ADR ; 
 reg   SEG7_CK ; 
 wire [3:0 ] SEG7_DAT  ; 
 
 ROM7  r7(
	.address (SEG7_ADR ) ,
	.clock   (SEG7_CK) ,
	.q (SEG7_DAT)  
	
	);
 */

endmodule
