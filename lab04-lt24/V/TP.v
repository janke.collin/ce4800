
module  TP ( 
    input frame_update, 
    input CLK , 
    input RESET_N ,
   
//-- LT24 LCD interface
     //-- output
    input LT24_CS_N,
    input LT24_RS,
    input LT24_WR_N,
    input LT24_RD_N,
    //-- inout 
    input [15:0]  LT24_D,
    
//-- ADC( for touch) 
    //  output 
    input LT24_ADC_DCLK,
    input LT24_ADC_CS_N,
    input  LT24_ADC_DIN,
    // input 
    input  LT24_ADC_BUSY,
    input  LT24_ADC_DOUT,
    input LT24_ADC_PENIRQ_N,
	 
	//test 
	 input [7:0]ST  , 
	
	 input  [15:0]BYTE , 
	 input [7:0]CNT ,
  input W_GO ,
input W_END ,
input  [7:0]	W_ST ,
input  [7:0] 	W_CNT 	,
input  [7:0]  IROM_ADDR ,
input         IROM_CK ,
input [15:0] IROM_DAT	 

);

endmodule
