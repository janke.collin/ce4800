module  WRITE_INIT (
   input  				RESET_N ,
	input      			PT_CK,
	input      			GO,
	input      [15:0]	DATA16,	
	input      [7:0]	COM,	
   input      [15:0]	BYTE , 
	output reg    [15:0]		LT24_D,
	output reg    LT24_RD_N,
	output        LT24_RESET_N,
	output reg    LT24_RS,
	output reg    LT24_WR_N ,
   output reg    LT24_CS_N	 , 
	
	output reg 			END_OK,
	
	//--for test 
	output reg [7:0]	ST ,
	output reg [7:0] 	CNT 
);

reg   [7:0]DELY ;

 //------------------------------------------------------------------------------------------
always @( negedge RESET_N or posedge  PT_CK )begin
if ( !RESET_N ) ST <=0;
else  begin 
case (ST)
 0: begin  //start 		      
	 CNT    <=0;
	 END_OK <=1;
    LT24_RD_N    <=1; 
	 LT24_CS_N    <=1; 	 
	 LT24_RS      <=1; 	 
	 LT24_WR_N    <=1; 
    LT24_D[15:0] <=16'hffff; 	 
	 //CNT <=0; 
	 if (GO) ST  <=30 ; 
    end	 
	  1: begin
	       ST<=2 ; 		  		  
			 LT24_D <= DATA16[7:0] ; 		  
			 if ( DATA16[15:8] == 8'hff ) LT24_RS <=0;
			   else LT24_RS <=1;
		  end 
	  2: begin
	       ST<=5 ;
	       LT24_CS_N <=0; 	 
		  end	 
		  
	  5: begin
	     ST<=6 ; 
	     LT24_WR_N   <=0; 		  
	  end
	  6: begin
	     LT24_WR_N <=1; 		  
		  ST <= 7 ; 
	  end
	  7: begin
	     ST<=30 ; 
        LT24_RD_N    <=1; 
	     LT24_CS_N    <=1; 	 
	     LT24_RS      <=1; 	 
	     LT24_WR_N    <=1; 
        LT24_D[15:0] <=16'hffff; 	 
	     END_OK <=1;	 
	     end
		  
//--END 		  
		30: begin
            if (!GO) ST  <=31;
          end
		31: begin  //
		      END_OK<=0;
				ST    <=1;	
			end
	  endcase 
 
 end
 end
 
 

endmodule
