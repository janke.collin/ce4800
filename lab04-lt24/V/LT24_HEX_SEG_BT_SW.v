module  LT24_HEX_SEG_BT_SW (   //for de1-soc  
		
//---DE1-SOC ---INT 		
      input      [6:0]  HEX0,
      input      [6:0]  HEX1,
      input      [6:0]  HEX2,
      input      [6:0]  HEX3,
      input      [6:0]  HEX4,
      input      [6:0]  HEX5,
		input      HEX0_DT,		
		input      HEX1_DT,
		input      HEX2_DT,
		input      HEX3_DT,
		input      HEX4_DT,		
		input      HEX5_DT,		
		
		
      output     [3:0]  KEY,
      input      [9:0]  LEDR,
      output     [9:0]  SW,
	
//---SYSTEM ----
     input  SEL , 
	  input  CLK_50,
     input  RESET_N ,
	
//--LT24 IF 	
	input 		          		LT24_ADC_BUSY,
	output		          		LT24_ADC_CS_N,
	output		          		LT24_ADC_DCLK,
	output		          		LT24_ADC_DIN,
	input 		          		LT24_ADC_DOUT,
	input 		          		LT24_ADC_PENIRQ_N,
	
	output		          		LT24_CS_N,
	inout	       [15:0]		   LT24_D,
	output		          		LT24_LCD_ON,
	output		          		LT24_RD_N,
	output		          		LT24_RESET_N,
	output		          		LT24_RS,
	output		          		LT24_WR_N
);

//=======================================================
//  REG/WIRE declarations
//=======================================================


//=======================================================
//  Structural coding
//=======================================================
wire      CLK_1M	 ;  
assign    LT24_LCD_ON  = RESET_N;


wire C50_1HZ ; 
CLOCKMEM ck4(  .CLK(CLK_50 ) ,.CLK_FREQ(50000000/6) ,.CK_1HZ (C50_1HZ) ) ; 

reg [23:0] MM ; 
reg RESET_N_DELAY  ; 

always @( negedge RESET_N or posedge C50_1HZ  )   
if (RESET_N ==0)  begin MM<=0 ;RESET_N_DELAY <=0; end
else   if ( MM < 1 )  MM<=MM+1 ;  
 else RESET_N_DELAY <=1;


//--------- COR TO 7-SEG --------  
wire   [23:0] CC ;
assign CC[23:0] =  { X_COORD[11:0] , Y_COORD[11:0] } ; 

wire [6:0]SEG0 ;
wire [6:0]SEG1 ;
wire [6:0]SEG2 ;
wire [6:0]SEG3 ;
wire [6:0]SEG4 ;
wire [6:0]SEG5 ; 

  
COR_7SEG  cr(
 .X_COORD ( X_COORD)  ,
 .Y_COORD ( Y_COORD)  ,
 .SEG0 (SEG0)  ,
 .SEG1 (SEG1)  ,
 .SEG2 (SEG2)  ,
 .SEG3 (SEG3)  ,
 .SEG4 (SEG4)  ,
 .SEG5 (SEG5)  
) ;
  
  
  
  
//---------LT24--------  
wire DRAW_READY; 

LT24_DRAWING  lt24( 
   .RESET_N     ( RESET_N_DELAY  ) , 
   .CLK_50      ( CLK_50), 
	.CLK_1M      ( CLK_1M      ),
	///*
	.LT24_D      ( LT24_D      ),
	.LT24_RD_N   ( LT24_RD_N   ),
	.LT24_RESET_N( LT24_RESET_N),
	.LT24_RS     ( LT24_RS     ),
	.LT24_WR_N   ( LT24_WR_N   ) ,
   .LT24_CS_N   ( LT24_CS_N )	,
	.DRAW_READY  ( DRAW_READY) ,

   .HEX0_D 	 (SEL?HEX0[6:0]:SEG0) ,
   .HEX1_D 	 (SEL?HEX1[6:0]:SEG1) ,
   .HEX2_D 	 (SEL?HEX2[6:0]:SEG2) ,
   .HEX3_D 	 (SEL?HEX3[6:0]:SEG3) ,
   .HEX4_D 	 (SEL?HEX4[6:0]:SEG4) ,
   .HEX5_D 	 (SEL?HEX5[6:0]:SEG5) ,
	

   .HEX0_DT(HEX0_DT),
   .HEX1_DT(HEX1_DT),
   .HEX2_DT(HEX2_DT),
   .HEX3_DT(HEX3_DT),
   .HEX4_DT(HEX4_DT),
   .HEX5_DT(HEX5_DT),	
	
	
	.LED   ( LEDR [9:0]   ) ,
	.SW    ( SW ) ,
	.KEY   ( KEY ) 		
	);

//---touch--- 
adc_spi_controller	adc(
					.iCLK         ( CLK_50) ,
					.iRST_n       ( RESET_N) ,
					.oADC_DIN     ( LT24_ADC_DIN) ,
					.oADC_DCLK    ( LT24_ADC_DCLK) ,
					.oADC_CS      ( LT24_ADC_CS_N), 
					.iADC_DOUT    ( LT24_ADC_DOUT) ,
					.iADC_BUSY    ( LT24_ADC_BUSY),
					.iADC_PENIRQ_n( LT24_ADC_PENIRQ_N),
					.oTOUCH_IRQ   (touch_t ),
					.oX_COORD     (X_COORD),
					.oY_COORD     (Y_COORD),
					.oNEW_COORD   () ,
); 	
					
wire	[11:0]	X_COORD;
wire	[11:0]	Y_COORD;
					
//---TOUCH  TO SWITCH --- 
wire [ 9:0 ] TSW ; 
TOUCH2SW (
   .RESET_N     (RESET_N_DELAY ),// RESET_N )   , 
   .CLK         (CLK_50) , 
	.DRAW_READY  ( DRAW_READY) ,	
   .TOUCH_IRQ   (LT24_ADC_PENIRQ_N ),
	.X_COORD     (X_COORD),
	.Y_COORD     (Y_COORD),
	.SW          ( SW[9:0]  ) 

) ; 

//---TOUCH  TO SWITCH --- 
wire [ 3:0 ] TKEY ; 
TOUCH2KEY ( 
   .RESET_N     ( RESET_N_DELAY ),// RESET_N ),
   .CLK         (	CLK_50 ) , 
	.DRAW_READY  ( DRAW_READY) ,
   .TOUCH_IRQ   ( LT24_ADC_PENIRQ_N),
	.X_COORD     ( X_COORD),
	.Y_COORD     (Y_COORD),
	.KEY         (  KEY[3:0]  ) 

) ; 

endmodule 