module COR_7SEG  (
input [11:0]  X_COORD  ,
input [11:0]  Y_COORD  ,
output [6:0]SEG0  ,
output [6:0]SEG1  ,
output [6:0]SEG2  ,
output [6:0]SEG3  ,
output [6:0]SEG4  ,
output [6:0]SEG5  
 
) ;
wire [23:0] CC ; 
assign CC= { X_COORD[11:0] , Y_COORD[11:0] } ; 
  
SEG7_LUT_V	v0(	.iDIG(CC[3:0  ])  , .oSEG(SEG0[6:0] ) ) ;   
SEG7_LUT_V	v1(	.iDIG(CC[7:4  ])  , .oSEG(SEG1[6:0] ) ) ;   
SEG7_LUT_V	v2(	.iDIG(CC[11:8 ])  , .oSEG(SEG2[6:0] ) ) ;   
SEG7_LUT_V	v3(	.iDIG(CC[15:12])  , .oSEG(SEG3[6:0] ) ) ;   
SEG7_LUT_V	v4(	.iDIG(CC[19:16])  , .oSEG(SEG4[6:0] ) ) ;   
SEG7_LUT_V	v5(	.iDIG(CC[23:20])  , .oSEG(SEG5[6:0] ) ) ;   

endmodule 