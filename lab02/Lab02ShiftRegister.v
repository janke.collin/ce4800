// Shift register function select
`define FN_LOAD 0 // Load
`define FN_LSL  1 // Logical Shift Left
`define FN_LSR  2 // Logical Shift Right
`define FN_ASR  3 // Arithmetic Shift Right

/**
 * Multi-function Shift Register
 *
 * Performs the specified shift function on the current output data on
 * each rising edge when enabled. Synchronous clear will override the selected
 * function and set all outputs to zero. Synchronous clear does nothing when the
 * component is disabled.
 */
module Lab02ShiftRegister
 #(parameter N = 4)
  (
    output [N-1:0] q, // Parallel data out
    output sh_out,    // Shift Out
    input clk,        // Clock
    input en_n,       // Enable (active low)
    input clr_n,      // Synchronous clear (active low)
    input [1:0] fn,   // Shift function select (use FN_* macros)
    input [N-1:0] d   // Parallel data in
);

    wire [N-1:0] q_int;
    assign q = q_int;

    // First Bit
    wire [3:0] d0_int;
    assign d0_int[`FN_LOAD] = d[0];
    assign d0_int[`FN_LSL]  = 0;
    assign d0_int[`FN_LSR]  = q_int[1];
    assign d0_int[`FN_ASR]  = q_int[1];

    ShiftUnit shifter0 (
        .q(q_int[0]),
        .sel(fn),
        .d(d0_int),
        .clk(clk),
        .en_n(en_n),
        .clr_n(clr_n)
    );

    // Middle Bits (Generic)
    genvar i;
    generate
    for (i = 1; i < N-1; i = i + 1) begin : ShiftUnitGen
        wire [3:0] d_int;
        assign d_int[`FN_LOAD] = d[i];
        assign d_int[`FN_LSL]  = q_int[i-1];
        assign d_int[`FN_LSR]  = q_int[i+1];
        assign d_int[`FN_ASR]  = q_int[i+1];

        ShiftUnit shifter (
            .q(q_int[i]),
            .sel(fn),
            .d(d_int),
            .clk(clk),
            .en_n(en_n),
            .clr_n(clr_n)
        );
    end
    endgenerate

    // Last Bit
    wire [3:0] dN_int;
    assign dN_int[`FN_LOAD] = d[N-1];
    assign dN_int[`FN_LSL]  = q_int[N-2];
    assign dN_int[`FN_LSR]  = 0;
    assign dN_int[`FN_ASR]  = q_int[N-1];

    ShiftUnit shifterN (
        .q(q_int[N-1]),
        .sel(fn),
        .d(dN_int),
        .clk(clk),
        .en_n(en_n),
        .clr_n(clr_n)
    );

    // Shift Out
    wire [3:0] sh_out_int;
    assign sh_out_int[`FN_LOAD] = 0;
    assign sh_out_int[`FN_LSL]  = q_int[N-1];
    assign sh_out_int[`FN_LSR]  = q_int[0];
    assign sh_out_int[`FN_ASR]  = q_int[0];

    ShiftUnit shifterShOut (
        .q(sh_out),
        .sel(fn),
        .d(sh_out_int),
        .clk(clk),
        .en_n(en_n),
        .clr_n(clr_n)
    );

endmodule
