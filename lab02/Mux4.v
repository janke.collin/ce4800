/**
 * 4-Port bit multiplexor
 */
module Mux4 (
    output y,        // Output data
    input [1:0] sel, // Data selection
    input [3:0] a    // Input data
);

    // Level 1 Muxes
    wire l1[1:0];
    mux2_prim(l1[0], sel[0], a[0], a[1]);
    mux2_prim(l1[1], sel[0], a[2], a[3]);

    // Level 2 (Output) Mux
    mux2_prim(y, sel[1], l1[0], l1[1]);

endmodule
