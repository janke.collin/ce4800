/**
 * Shift Register Unit
 * Glues a Mux4 and DFF together (Mux out -> DFF in)
 */
module ShiftUnit (
    output q,
    input [1:0] sel,
    input [3:0] d,
    input clk, en_n, clr_n
);
    wire d_sel;

    Mux4 dataMux (
        .y(d_sel),
        .sel(sel),
        .a(d)
    );

    dff_prim(q, clk, en_n, clr_n, d_sel);

endmodule
