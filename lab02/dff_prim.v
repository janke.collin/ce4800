/**
 * Rising edge triggered D Flip-Flop
 * with active low enable and synchronous clear
 *
 * Note: The synchronous clear has no effect unless the DFF is enabled
 */
primitive dff_prim (
    output reg q, // Output state
    input  clk,   // Clock
    input  en_n,  // Enable (active low)
    input  clr_n, // Synchronous clear (active low)
    input  d      // Input data
);
    table
    //  clk  en_n  clr_n  d : cs : q
        r    0     1      0 : ?  : 0; // d->q if enabled and not cleared
        r    0     1      1 : ?  : 1; // d->q if enabled and not cleared
        r    0     0      ? : ?  : 0; // 0->q if enabled and cleared
        f    ?     ?      ? : ?  : -; // Don't change output on falling edge
        r    1     ?      ? : ?  : -; // Don't change output if not enabled
        ?    *     ?      ? : ?  : -; // Don't change output if en_n changes
        ?    ?     ?      * : ?  : -; // Don't change output if d changes
        ?    ?     *      ? : ?  : -; // Don't change output if clr_n changes
    endtable
endprimitive
