/**
 * 2-Port bit multiplexor
 */
primitive mux2_prim (
    output y,  // Output data
    input sel, // Data selection
    input a,   // Input A (sel=0)
    input b    // Input B (sel=1)
);
    table
    //  select  a  b : mux_out
        0       0  ? : 0;
        0       1  ? : 1;
        1       ?  0 : 0;
        1       ?  1 : 1;
        ?       0  0 : 0;
        ?       1  1 : 1;
    endtable
endprimitive
