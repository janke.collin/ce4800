// Shift register function select
`define FN_LOAD 0 // Load
`define FN_LSL  1 // Logical Shift Left
`define FN_LSR  2 // Logical Shift Right
`define FN_ASR  3 // Arithmetic Shift Right

module Lab02ShiftRegister_Test ();

    parameter N = 4;

    wire [N-1:0] q;
    wire sh_out;
    reg clk;
    reg en_n;
    reg clr_n;
    reg [1:0] fn;
    reg [N-1:0] d;

    Lab02ShiftRegister #(.N(N)) uut (
        .q(q),
        .sh_out(sh_out),
        .clk(clk),
        .en_n(en_n),
        .clr_n(clr_n),
        .fn(fn),
        .d(d)
    );

    initial $monitor("[%02t] clk=%b en_n=%b clr_n=%b fn=%s d=%4b q=%4b sh=%b",
        $time, clk, en_n, clr_n,
        (fn[1] ? (fn[0] ? "ASR" : "LSR") : (fn[0] ? "LSL" : "LOAD")),
        d, q, sh_out);

    always begin
        #2 clk = 1;
        #2 clk = 0;
    end

    initial begin
        clk = 0; en_n = 0; clr_n = 1;

        // Load
        fn = `FN_LOAD; d = 4'b0000;
        #3 assert(q === 4'b0000);
           assert(sh_out === 0);
        #2 d = 4'b1111;
        #2 assert(q === 4'b1111);
           assert(sh_out === 0);

        // Clear/Enable
        #1 clr_n = 0;
        #1 assert(q === 4'b1111);
        #2 assert(q === 4'b0000);
        #2 en_n = 1; clr_n = 1;
        #2 assert(q === 4'b0000);
        #2 en_n = 0; d = 4'b0101;
        #2 assert(q === 4'b0101);
        #2 en_n = 1;
        #2 assert(q === 4'b0101);
        #4 assert(q === 4'b0101);

        // LSL
        #2 en_n = 0; clr_n = 1;
           fn = `FN_LOAD;
           d = 4'b1001;
        #2 assert(q === 4'b1001);
           assert(sh_out === 0);
        #2 fn = `FN_LSL;
        #2 assert(q === 4'b0010);
           assert(sh_out === 1);
        #4 assert(q === 4'b0100);
           assert(sh_out === 0);
        #2 en_n = 1;
        #2 assert(q === 4'b0100);
           assert(sh_out === 0);
        #2 en_n = 0;
        #2 assert(q === 4'b1000);
           assert(sh_out === 0);
        #4 assert(q === 4'b0000);
           assert(sh_out === 1);
        #4 assert(q === 4'b0000);
           assert(sh_out === 0);

        // LSR
        #2 en_n = 0; clr_n = 1;
           fn = `FN_LOAD;
           d = 4'b1001;
        #2 assert(q === 4'b1001);
           assert(sh_out === 0);
        #2 fn = `FN_LSR;
        #2 assert(q === 4'b0100);
           assert(sh_out === 1);
        #2 en_n = 1;
        #2 assert(q === 4'b0100);
           assert(sh_out === 1);
        #2 en_n = 0;
        #2 assert(q === 4'b0010);
           assert(sh_out === 0);
        #4 assert(q === 4'b0001);
           assert(sh_out === 0);
        #4 assert(q === 4'b0000);
           assert(sh_out === 1);
        #4 assert(q === 4'b0000);
           assert(sh_out === 0);

        // ASR
        #2 en_n = 0; clr_n = 1;
           fn = `FN_LOAD;
           d = 4'b1001;
        #2 assert(q === 4'b1001);
           assert(sh_out === 0);
        #2 fn = `FN_ASR;
        #2 assert(q === 4'b1100);
           assert(sh_out === 1);
        #4 assert(q === 4'b1110);
           assert(sh_out === 0);
        #4 assert(q === 4'b1111);
           assert(sh_out === 0);
        #4 assert(q === 4'b1111);
           assert(sh_out === 1);
        #4 assert(q === 4'b1111);
           assert(sh_out === 1);

        #2 en_n = 0; clr_n = 1;
           fn = `FN_LOAD;
           d = 4'b0110;
        #2 assert(q === 4'b0110);
           assert(sh_out === 0);
        #2 fn = `FN_ASR;
        #2 assert(q === 4'b0011);
           assert(sh_out === 0);
        #4 assert(q === 4'b0001);
           assert(sh_out === 1);
        #4 assert(q === 4'b0000);
           assert(sh_out === 1);
        #4 assert(q === 4'b0000);
           assert(sh_out === 0);
        #4 assert(q === 4'b0000);
           assert(sh_out === 0);


        #2 $finish;
    end

endmodule
