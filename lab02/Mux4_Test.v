module Mux4_Test ();

    wire y;
    reg [1:0] sel;
    reg [3:0] a;

    Mux4 uut (
        .y(y),
        .sel(sel),
        .a(a)
    );

    initial $monitor("%4b[%1d] -> %b", a, sel, y);

    initial #16 $finish;

    initial begin
        sel = 0;
        a = 4'b0000;
        #1 assert(y === 0);
        #1 a = 4'b0001;
        #1 assert(y === 1);
        #1 sel = 1;
        #1 assert(y === 0);
        #1 a = 4'b0010;
        #1 assert(y === 1);
        #1 sel = 2;
        #1 assert(y === 0);
        #1 a = 4'b0100;
        #1 assert(y === 1);
        #1 sel = 3;
        #1 assert(y === 0);
        #1 a = 4'b1000;
        #1 assert(y === 1);
    end

endmodule
