module dff_prim_Test ();

    wire q;
    reg clk;
    reg en_n;
    reg clr_n;
    reg d;

    dff_prim uut (q, clk, en_n, clr_n, d);

    initial $monitor("[%2t] clk=%b clr=%b en_n=%b %b-%s->%b",
        $time, clk, clr_n, en_n, d, ((clk && !en_n) ? (clr_n ? "-" : "0") : " "), q
    );

    always begin
        #2 clk = 1;
        #2 clk = 0;
    end

    initial begin
        clk = 0;
        en_n = 0; clr_n = 1; d = 0;
        // Basic (no enable or rst)
        #3 assert(q === 0); // high
        #2 d = 1;           // low
        #2 assert(q === 1); // high
        #4 assert(q === 1); // high

        // Clear
        #1 clr_n = 0;       // falling edge
        #1 assert(q === 1); // low, still 1
        #2 assert(q === 0); // high, cleared to 0
        #1 d = 0;           // falling edge
        #1 assert(q === 0); // low, still 0
        #2 assert(q === 0); // high, still 0

        // Enable
        #1 en_n = 1; clr_n = 1; d = 1; // falling edge
        #1 assert(q === 0); // low, still 0
        #2 assert(q === 0); // high, still 0
        #1 en_n = 0;        // falling edge
        #1 assert(q === 0); // low, don't set data until rising edge
        #2 assert(q === 1); // high
        #1 en_n = 1; d = 0; // falling edge, disable
        #1 assert(q === 1); // low, still 1
        #2 assert(q === 1); // high, still 1

        // Enable/Clear precedence
        #1 clr_n = 0; d = 1;// falling edge, set clr
        #1 assert(q === 1); // low, still 1
        #2 assert(q === 1); // high, still 1
        #1 en_n = 0;        // falling edge, enable
        #1 assert(q === 1); // low, still 1
        #2 assert(q === 0); // high, cleared to zero

        #2 $finish;
    end

endmodule
