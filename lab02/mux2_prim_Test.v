module mux2_prim_Test ();

    wire y;
    reg sel;
    reg a;
    reg b;

    mux2_prim uut (
        y,
        sel,
        a,
        b
    );

    initial $monitor("%b%b[%b] -> %b", b, a, sel, y);

    initial #20 $finish;

    initial begin
        sel = 0; a = 0; b = 0;

        // Basic Functionality
        #1 assert(y === 0);
        #1 a = 1;
        #1 assert(y === 1);
        #1 sel = 1;
        #1 assert(y === 0);
        #1 b = 1;
        #1 assert(y === 1);

        // With unknowns
        #1 sel = 1'bx; a = 0; b = 0;
        #1 assert(y === 0);
        #1 sel = 1'bx; a = 1; b = 1;
        #1 assert(y === 1);

        #1 sel = 1; a = 1'bx; b = 0;
        #1 assert(y === 0);
        #1 sel = 1; a = 1'bx; b = 1;
        #1 assert(y === 1);

        #1 sel = 0; a = 0; b = 1'bx;
        #1 assert(y === 0);
        #1 sel = 0; a = 1; b = 1'bx;
        #1 assert(y === 1);
    end

endmodule
