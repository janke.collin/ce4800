module PriorityEncoder_Test ();

    parameter N = 2;

    wire [N-1 : 0] y;
    wire valid;
    reg [(2**N)-1 : 0] a;
    reg en_n;

    PriorityEncoder #(.N(N)) uut (
        .y(y),
        .valid(valid),
        .a(a),
        .en_n(en_n)
    );

    initial $monitor("[%02t] en_n=%b a=%4b -> y=%d (%2b) valid=%b",
        $time, en_n, a, y, y, valid);

    initial begin
        en_n = 0; a = 0;
        #1 assert(!valid);

        #1 a = 4'b0001;
        #1 assert(y === 0);
           assert(valid);

        #1 a = 4'b0010;
        #1 assert(y === 1);
           assert(valid);

        #1 a = 4'b0100;
        #1 assert(y === 2);
           assert(valid);

        #1 a = 4'b1000;
        #1 assert(y === 3);
           assert(valid);

        #1 en_n = 1;
        #1 assert(!valid);

        #1 a = 4'b0000;
        #1 assert(!valid);

        #1 a = 4'bxxxx;
        #1 assert(!valid);
        #1 $finish;
    end

endmodule
