module Decoder_Test ();

    parameter N = 2;

    wire [(2**N)-1 : 0] y;
    reg [N-1 : 0] a;
    reg en_n;

    Decoder #(.N(N)) uut (
        .y(y),
        .a(a),
        .en_n(en_n)
    );

    initial $monitor("[%02t] en_n=%b a=%d (%2b) -> y=%4b",
        $time, en_n, a, a, y);

    initial begin
        en_n = 0; a = 0;
        #1 assert(y === 4'b0001);
        #1 a = 1;
        #1 assert(y === 4'b0010);
        #1 a = 2;
        #1 assert(y === 4'b0100);
        #1 a = 3;
        #1 assert(y === 4'b1000);
        #1 en_n = 1;
        #1 assert(y === 4'b0000);
        #1 a = 4'bxxxx;
        #1 assert(y === 4'b0000);
        #1 $finish;
    end

endmodule
