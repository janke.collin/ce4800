/**
 * A generic width and generic port-count bus multiplexor implemented
 * with a basic assignment.
 *
 * Parameters:
 *   N: data width
 *   SEL_BITS: number of selection bits (number of inputs is 2**SEL_BITS)
 *
 * Inputs:
 *   a: input data as array of N-bit values (flattened 2D bit array).
 *      Port 0 is N-1 down to 0, port 1 is 2N-1 down to N, port 3 is
 *      3N-1 down to 2N, etc.
 *   sel: port selector
 *
 * Outputs:
 *   y: N-bit output data
 */
module BusMuxDataflow
 #(
     parameter N = 8,
     parameter SEL_BITS = 2
  )
  (
    output [N-1 : 0] y,
    input [((2**SEL_BITS) * N)-1 : 0] a,
    input [SEL_BITS-1 : 0] sel
);

    assign y = a[((sel+1)*N)-1 -: N];

endmodule
