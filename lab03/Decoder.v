/**
 * A generic-width decoder with active-low enable. Converts a binary input to
 * a one-hot output.
 *
 * Examples:
 *   a: 00 -> y: 0001
 *   a: 01 -> y: 0010
 *
 * Parameters:
 *   N: number of input bits (the number of output bits is 2**N)
 *
 * Inputs:
 *   en_n: Active-low enable
 *   a: N-bit binary input
 *
 * Outputs:
 *   y: 2**N-bit one-hot output, or 0 if en_n is 1
 */
module Decoder
 #(parameter N = 4)
  (
    output reg [(2**N)-1 : 0] y,
    input [N-1 : 0] a,
    input en_n
);

    always @(a, en_n) begin
        if (en_n == 0)
        begin
            y = 0;
            y[a] = 1;
        end
        else
            y = 0;
    end

endmodule
