/**
 * A generic width and generic port-count bus multiplexor implemented
 * structurally with a SEL_BITS-to-1 mux for each data bit.
 *
 * Parameters:
 *   N: data width
 *   SEL_BITS: number of selection bits (number of inputs is 2**SEL_BITS)
 *
 * Inputs:
 *   a: input data as array of N-bit values (flattened 2D bit array).
 *      Port 0 is N-1 down to 0, port 1 is 2N-1 down to N, port 3 is
 *      3N-1 down to 2N, etc.
 *   sel: port selector
 *
 * Outputs:
 *   y: N-bit output data
 */
module BusMuxStruct
 #(
     parameter N = 8,
     parameter SEL_BITS = 2
  )
  (
    output [N-1 : 0] y,
    input [((2**SEL_BITS) * N)-1 : 0] a,
    input [SEL_BITS-1 : 0] sel
);

    genvar i, j;
    generate
        for (i = 0; i < N; i = i + 1) begin : MuxGen

            wire [N-1:0] a_int;
            for (j = 0; j < 2**SEL_BITS; j = j + 1) begin : MuxDataGen
                assign a_int[j] = a[j*N + i];
            end

            MuxN #(.SEL_BITS(SEL_BITS)) mux (
                .y(y[i]),
                .a(a_int),
                .sel(sel)
            );
        end
    endgenerate

endmodule
