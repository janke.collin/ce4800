/**
 * A generic-width unsigned integer comparator
 *
 * Parameters:
 *   N: width of integer inputs in bits
 *
 * Inputs:
 *   a: First N-bit integer
 *   b: Second N-bit integer
 *
 * Outputs:
 *   lt: 1 if a < b, else 0
 *   gt: 1 if a > b, else 0
 *   eq: 1 if a = b, else 0
 */
module Comparator
 #(parameter N = 32)
  (
    output reg lt, gt, eq,
    input [N-1:0] a, b
);

    always @(a, b) begin
        eq = (a == b);
        lt = (a < b);
        gt = !eq && !lt;
    end

endmodule
