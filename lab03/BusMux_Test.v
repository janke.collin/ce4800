module BusMux_Test ();

    parameter N = 4;
    parameter SEL_BITS = 2;

    wire [N-1 : 0] y;
    reg [((2**SEL_BITS) * N)-1 : 0] a;
    reg [SEL_BITS-1 : 0] sel;

    BusMuxRecursive #(.N(N),.SEL_BITS(SEL_BITS)) uut (
        .y(y),
        .a(a),
        .sel(sel)
    );

    initial $monitor("[%02t] sel=%2d a=%4x -> y=%x",
        $time, sel, a, y);

    initial begin
        sel = 0; a = 16'hdcba;
        #1 assert(y === 4'ha);
        #1 sel = 1;
        #1 assert(y === 4'hb);
        #1 sel = 2;
        #1 assert(y === 4'hc);
        #1 sel = 3;
        #1 assert(y === 4'hd);

        #1 sel = 0;
           a = 16'h000f;
        #1 assert(y === 4'hf);
        #1 sel = 1;
        #1 assert(y === 4'h0);
        #1 a = 16'h00f0;
        #1 assert(y === 4'hf);
        #1 sel = 2;
        #1 assert(y === 4'h0);
        #1 a = 16'h0f00;
        #1 assert(y === 4'hf);
        #1 sel = 3;
        #1 assert(y === 4'h0);
        #1 a = 16'hf000;
        #1 assert(y === 4'hf);
        #1 sel = 0;
        #1 assert(y === 4'h0);
        #1 $finish;
    end

endmodule
