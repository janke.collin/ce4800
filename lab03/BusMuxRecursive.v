/**
 * A generic width and generic port-count bus multiplexor implemented
 * with recursive 2:1 bus muxes.
 *
 * Parameters:
 *   N: data width
 *   SEL_BITS: number of selection bits (number of inputs is 2**SEL_BITS)
 *
 * Inputs:
 *   a: input data as array of N-bit values (flattened 2D bit array).
 *      Port 0 is N-1 down to 0, port 1 is 2N-1 down to N, port 3 is
 *      3N-1 down to 2N, etc.
 *   sel: port selector
 *
 * Outputs:
 *   y: N-bit output data
 */
module BusMuxRecursive #(
     parameter N = 8,
     parameter SEL_BITS = 3
  )
  (
    output [N-1 : 0] y,
    input [((2**SEL_BITS) * N)-1 : 0] a,
    input [SEL_BITS-1 : 0] sel
);

    parameter DATAIN_SIZE = $size(a);
    parameter HALF_DATAIN_SIZE = DATAIN_SIZE / 2;

    // Based on most significant selection bit, set output to
    // output of b (1) or a (0)
    wire [N-1 : 0] a_out, b_out;
    assign y = sel[SEL_BITS-1] ? b_out : a_out;

    // Split input data in half
    wire [HALF_DATAIN_SIZE-1 : 0] a_data, b_data;
    assign a_data  = a[HALF_DATAIN_SIZE-1 : 0];
    assign b_data  = a[DATAIN_SIZE-1 : HALF_DATAIN_SIZE];

    generate
        if (SEL_BITS > 1) begin
            // If more than 1 selection bit, chop off MSB, and recurse to next level
            // Send lower half of data to muxA and upper half to muxB
            BusMuxRecursive #(
                .N(N),
                .SEL_BITS(SEL_BITS-1)
                ) muxA (
                    .y(a_out),
                    .a(a_data),
                    .sel(sel[SEL_BITS-2 : 0])
                );

            BusMuxRecursive #(
                .N(N),
                .SEL_BITS(SEL_BITS-1)
                ) muxB (
                    .y(b_out),
                    .a(b_data),
                    .sel(sel[SEL_BITS-2 : 0])
                );
        end
        else begin
            // Base case: 1 selection bit
            assign a_out = a_data;
            assign b_out = b_data;
        end

    endgenerate

endmodule
