module MuxN
 #(parameter SEL_BITS = 4)
  (
    output  y,
    input [(2**SEL_BITS)-1 : 0] a,
    input [SEL_BITS-1 : 0] sel
);

    assign y = a[sel];

endmodule
