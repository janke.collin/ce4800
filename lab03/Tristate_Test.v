module Tristate_Test ();

    parameter N = 4;

    wire [N-1 : 0] y;
    reg [N-1 : 0] a;
    reg en_n;

    Tristate #(.N(N)) uut (
        .y(y),
        .a(a),
        .en_n(en_n)
    );

    initial $monitor("[%02t] en_n=%b a=%4b -> y=%4b",
        $time, en_n, a, y);

    initial begin
        en_n = 0; a = 0;
        #1 assert(y === 4'b0000);
        #1 a = 1;
        #1 assert(y === 4'b0001);
        #1 a = 15;
        #1 assert(y === 4'b1111);
        #1 en_n = 1;
        #1 assert(y === 4'bzzzz);
        #1 a = 0;
        #1 assert(y === 4'bzzzz);
        #1 a = 4'bxxxx;
        #1 assert(y === 4'bzzzz);
        #1 $finish;
    end

endmodule
