/**
 * A generic-width priority encoder with active-low enable. Returns the number
 * of the lowest input pin that is 1.
 *
 * Examples:
 *   a: 0000 -> y: xx valid: 0
 *   a: 0001 -> y: 00 valid: 1
 *   a: 0010 -> y: 01 valid: 1
 *
 * Parameters:
 *   N: number of output bits (the number of input bits is 2**N)
 *
 * Inputs:
 *   en_n: Active-low enable
 *   a: 2**N-bit input data
 *
 * Outputs:
 *   y: N-bit binary output, or 0 if en_n is 1.
 *   valid: 1 if y is valid, else 0. y is invalid if a = 0 or en_n = 1.
 */
module PriorityEncoder
 #(parameter N = 2)
  (
    output reg [N-1 : 0] y,
    output reg valid,
    input [(2**N)-1 : 0] a,
    input en_n
);

    integer i;
    always @(a, en_n) begin
        i = 0;
        if (en_n == 0)
        begin
            valid = (a != 0);
            y = 0;
            for (i = (2**N)-1; i >= 0; i = i - 1) begin
                if (a[i] == 1) begin
                    y = i;
                end
            end
        end
        else
        begin
            valid = 0;
            y = 0;
        end
    end


endmodule
