/**
 * A generic-width tristate buffer
 *
 * Parameters:
 *   N: data width
 *
 * Inputs:
 *   en_n: Active-low enable
 *   a: input data
 *
 * Outputs:
 *   y: a if en_n is 0, else high impedance (z)
 */
module Tristate
 #(parameter N = 4)
  (
    output reg [N-1 : 0] y,
    input [N-1 : 0] a,
    input en_n
);

    always @(a, en_n) begin
        if (en_n == 0)
            y = a;
        else
            y = {N{1'bz}};
    end

endmodule
