module Comparator_Test ();

    parameter N = 4;

    wire lt;
    wire gt;
    wire eq;
    reg [N-1:0] a;
    reg [N-1:0] b;

    Comparator #(.N(N)) uut (
        .lt(lt),
        .gt(gt),
        .eq(eq),
        .a(a),
        .b(b)
    );

    wire lt_only =  lt & !gt & !eq;
    wire gt_only = !lt &  gt & !eq;
    wire eq_only = !lt & !gt &  eq;

    initial $monitor("[%02t] %2d (%4b) %s%s%s %2d (%4b)",
        $time, a, a, (lt ? "<" : ""), (eq ? "=" : ""), (gt ? ">" : ""), b, b);

    initial begin
        a = 0; b = 0;
        #1 assert(eq_only);
        #1 a = 1;
        #1 assert(gt_only);
        #1 a = 8;
        #1 assert(gt_only);
        #1 b = 8;
        #1 assert(eq_only);
        #1 b = 15;
        #1 assert(lt_only);
        #1 a = 0;
        #1 assert(lt_only);
        #1 b = 1;
        #1 assert(lt_only);
        #1 $finish;
    end

endmodule
