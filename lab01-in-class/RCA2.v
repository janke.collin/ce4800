module RCA2(
    output [1:0]sum,
    output cout,
    input [1:0] a,
    input [1:0] b
);

    wire c1;

    HalfAdder HA (c1, sum[0], a[0], b[0]);
    FullAdder FA (
        .a(a[1]),
        .b(b[1]),
        .cin(c1),
        .sum(sum[1]),
        .cout(cout));

endmodule
