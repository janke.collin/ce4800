module RCA2_Test (

);

    wire [1:0]sum;
    wire cout;
    reg [1:0] a;
    reg [1:0] b;

    RCA2 uut (
        .a(a),
        .b(b),
        .sum(sum),
        .cout(cout)
    );

    parameter time_out = 50;

    initial $monitor("%2b + %2b = %b%2b (%d + %d = %d + %b)",
        a,b,cout,sum,
        a,b,sum,cout
    );

    initial #time_out $finish;

    initial begin
        #10 a = 2'd0; b = 2'd3;
        #10 a = 2'd1; b = 2'd3;
        #10 a = 2'd3; b = 2'd1;
        #10 a = 2'd3; b = 2'd2;
    end


endmodule
