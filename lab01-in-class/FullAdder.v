
module FullAdder (output cout, sum, input a, b, cin);

    // xor(sum, a, b, cin);

    // // cout = (a)(b) or (a)(cin) or (b)(cin)
    // wire w1, w2, w3;
    // and(w1, a, b);
    // and(w2, a, cin);
    // and(w3, b, cin);
    // or(cout, w1, w2, w3);

    assign sum = a ^ b ^ cin;
    assign cout = (a & b) | (a & cin) | (b & cin);

endmodule
