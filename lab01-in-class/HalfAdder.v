// Half Adder

module HalfAdder (cout, sum, a, b);

    output cout, sum;
    input a, b;

    xor(sum, a, b);
    and(cout, a, b);

endmodule
