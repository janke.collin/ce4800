module HalfAdder_Test();

    reg a, b;
    wire sum, cout;

    HalfAdder UUT (
        .a(a),
        .b(b),
        .sum(sum),
        .cout(cout)
    );

    initial begin
        #50 $finish; // 100 units of time
    end

    initial begin
        #10 a=0; b=0; // delay 10 units
        #10 a=0; b=1;
        #10 a=1; b=0;
        #10 a=1; b=1;
    end

    initial $monitor("%b + %b = %b %b", a, b, cout, sum);

endmodule
