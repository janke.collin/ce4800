module ShiftReg (
    output reg a,
    input e,
    input clk, rst
);

    // e -> d c b a

    reg d, c, b;

    always @(posedge clk, posedge rst) begin
        if (rst == 1) begin
            a = 0;
            b = 0;
            c = 0;
            d = 0;
        end
        else begin
            // // This works, but requires correct order
            // a = b;
            // b = c;
            // c = d;
            // d = e;

            // // This is wrong... these lines are sequential,
            // // so is equivalent to a = e
            // // In simulation, d,c,b unknown! (optimized out)
            // d = e;
            // c = d;
            // b = c;
            // a = b;

            // Using non-blocking assignment
            d <= e;
            c <= d;
            b <= c;
            a <= b;
        end
    end



endmodule
