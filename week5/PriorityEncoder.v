module PriorityEncoder (
    output reg [1:0] y,
    output reg valid,
    input [3:0] a
);

    always @(a) begin

        valid = |a; // a[3] | a[2] | ...

        casex (a)
            4'b1xxx : y = 3;
            4'b01xx : y = 2;
            4'b001x : y = 1;
            4'b0001 : y = 0;
            default : y = 2'bx;
        endcase
    end

endmodule
