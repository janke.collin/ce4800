module PriToOneHot (
    output reg [3:0] y,
    output reg valid,
    input [3:0] a
);

    parameter ZERO  = 4'b0001;
    parameter ONE   = 4'b0010;
    parameter TWO   = 4'b0100;
    parameter THREE = 4'b1000;

    always @(a) begin

        valid = |a; // a[3] | a[2] | ...

        casex (a)
            4'b1xxx : y = THREE;
            4'b01xx : y = TWO;
            4'b001x : y = ONE;
            4'b0001 : y = ZERO;
            default : y = 4'bx;
        endcase
    end

endmodule
