module ParityChecker_Test ();

    wire complete;
    wire dataValid;
    wire [7:0] dataOut;

    wire [3:0] bitsReceived;
    wire odd;

    reg dataIn;
    reg parity_evenOdd;
    reg dataRecv;
    reg clk;
    reg rst_n;

    ParityChecker uut (
        .complete(complete),
        .dataValid(dataValid),
        .dataOut(dataOut),
        .dataIn(dataIn),
        .parity_evenOdd(parity_evenOdd),
        .dataRecv(dataRecv),
        .clk(clk),
        .rst_n(rst_n)
    );

    assign bitsReceived = uut.bitsReceived;
    assign odd = uut.odd;

    always @(clk, rst_n) begin
        $display("[%02t] clk=%b rst_n=%b dataRecv=%b parity=%s dataIn=%b : odd=%b bitsReceived=%d : dataOut=%02x (%08b) complete=%b valid=%b",
            $time, clk, rst_n, dataRecv, (parity_evenOdd ? "ODD" : "EVEN"), dataIn,
            odd, bitsReceived,
            dataOut, dataOut, complete, dataValid);
    end

    always begin
           clk = 1;
        #2 clk = 0;
        #2 clk = 1;
    end

    initial begin
        rst_n = 0;
        dataIn = 1;
        dataRecv = 1;
        parity_evenOdd = 1;
        #1 assert(dataOut === 0);
           assert(!complete);
           assert(!dataValid);

        #2 rst_n = 1;

        // 11110000 0 should be invalid for odd parity
           dataRecv = 1; dataIn = 1;
        #2 assert(dataOut[  0] == 1'h01); assert(!complete); assert(!dataValid);
        #4 assert(dataOut[1:0] == 2'h03); assert(!complete); assert(!dataValid);
        #4 assert(dataOut[2:0] == 3'h07); assert(!complete); assert(!dataValid);
        #4 assert(dataOut[3:0] == 4'h0f); assert(!complete); assert(!dataValid);
        #2 dataRecv = 0; dataIn = 0;
        #2 assert(dataOut[3:0] == 4'h0f); assert(!complete); assert(!dataValid);
        #4 assert(dataOut[3:0] == 4'h0f); assert(!complete); assert(!dataValid);
        #2 dataRecv = 1;
        #2 assert(dataOut[4:0] == 5'h1e); assert(!complete); assert(!dataValid);
        #4 assert(dataOut[5:0] == 6'h3c); assert(!complete); assert(!dataValid);
        #4 assert(dataOut[6:0] == 7'h78); assert(!complete); assert(!dataValid);
        #4 assert(dataOut[7:0] == 8'hf0); assert(!complete); assert(!dataValid);
        #4 assert(dataOut[7:0] == 8'hf0); assert(complete);  assert(!dataValid);

        // 00001111 1 should be valid for odd parity
        #2 dataRecv = 1; dataIn = 0;
        #2 assert(dataOut[  0] == 1'h00); assert(!complete); assert(!dataValid);
        #4 assert(dataOut[1:0] == 2'h00); assert(!complete); assert(!dataValid);
        #4 assert(dataOut[2:0] == 3'h00); assert(!complete); assert(!dataValid);
        #4 assert(dataOut[3:0] == 4'h00); assert(!complete); assert(!dataValid);
        #2 dataRecv = 0; dataIn = 1;
        #2 assert(dataOut[3:0] == 4'h00); assert(!complete); assert(!dataValid);
        #4 assert(dataOut[3:0] == 4'h00); assert(!complete); assert(!dataValid);
        #2 dataRecv = 1;
        #2 assert(dataOut[4:0] == 5'h01); assert(!complete); assert(!dataValid);
        #4 assert(dataOut[5:0] == 6'h03); assert(!complete); assert(!dataValid);
        #4 assert(dataOut[6:0] == 7'h07); assert(!complete); assert(!dataValid);
        #4 assert(dataOut[7:0] == 8'h0f); assert(!complete); assert(!dataValid);
        #4 assert(dataOut[7:0] == 8'h0f); assert(complete);  assert(dataValid);

        // Make sure all outputs hold if disabled
        #2 dataRecv = 0;
        #2 assert(dataOut[7:0] == 8'h0f); assert(complete); assert(dataValid);

        // 01010101 0 should be invalid for odd parity
        #2 dataRecv = 1; dataIn = 0;
        #2 assert(dataOut[  0] == 1'h00); assert(!complete); assert(!dataValid);
        #2 dataIn = 1;
        #2 assert(dataOut[1:0] == 2'h01); assert(!complete); assert(!dataValid);
        #2 dataIn = 0;
        #2 assert(dataOut[2:0] == 3'h02); assert(!complete); assert(!dataValid);
        #2 dataIn = 1;
        #2 assert(dataOut[3:0] == 4'h05); assert(!complete); assert(!dataValid);
        #2 dataIn = 0;
        #2 assert(dataOut[4:0] == 5'h0a); assert(!complete); assert(!dataValid);
        #2 dataIn = 1;
        #2 assert(dataOut[5:0] == 6'h15); assert(!complete); assert(!dataValid);
        #2 dataIn = 0;
        #2 assert(dataOut[6:0] == 7'h2a); assert(!complete); assert(!dataValid);
        #2 dataIn = 1;
        #2 assert(dataOut[7:0] == 8'h55); assert(!complete); assert(!dataValid);
        #2 dataIn = 0;
        #2 assert(dataOut[7:0] == 8'h55); assert(complete);  assert(!dataValid);

        // 10101010 1 should be valid for odd parity
        #2 dataIn = 1;
        #2 assert(dataOut[  0] == 1'h01); assert(!complete); assert(!dataValid);
        #2 dataIn = 0;
        #2 assert(dataOut[1:0] == 2'h02); assert(!complete); assert(!dataValid);
        #2 dataIn = 1;
        #2 assert(dataOut[2:0] == 3'h05); assert(!complete); assert(!dataValid);
        #2 dataIn = 0;
        #2 assert(dataOut[3:0] == 4'h0a); assert(!complete); assert(!dataValid);
        #2 dataIn = 1;
        #2 assert(dataOut[4:0] == 5'h15); assert(!complete); assert(!dataValid);
        #2 dataIn = 0;
        #2 assert(dataOut[5:0] == 6'h2a); assert(!complete); assert(!dataValid);
        #2 dataIn = 1;
        #2 assert(dataOut[6:0] == 7'h55); assert(!complete); assert(!dataValid);
        #2 dataIn = 0;
        #2 assert(dataOut[7:0] == 8'haa); assert(!complete); assert(!dataValid);
        #2 dataIn = 1;
        #2 assert(dataOut[7:0] == 8'haa); assert(complete);  assert(dataValid);

        ///// Even parity
        // Reset
        #1 rst_n = 0;
        #1 assert(dataOut === 0);
           assert(!complete);
           assert(!dataValid);
        #2 rst_n = 1;

        // 11110000 0 should be valid for even parity
        #2 dataRecv = 1; parity_evenOdd = 0; dataIn = 1;
        #2 assert(dataOut[  0] == 1'h01); assert(!complete); assert(!dataValid);
        #4 assert(dataOut[1:0] == 2'h03); assert(!complete); assert(!dataValid);
        #4 assert(dataOut[2:0] == 3'h07); assert(!complete); assert(!dataValid);
        #4 assert(dataOut[3:0] == 4'h0f); assert(!complete); assert(!dataValid);
        #2 dataIn = 0;
        #2 assert(dataOut[4:0] == 5'h1e); assert(!complete); assert(!dataValid);
        #4 assert(dataOut[5:0] == 6'h3c); assert(!complete); assert(!dataValid);
        #4 assert(dataOut[6:0] == 7'h78); assert(!complete); assert(!dataValid);
        #4 assert(dataOut[7:0] == 8'hf0); assert(!complete); assert(!dataValid);
        #4 assert(dataOut[7:0] == 8'hf0); assert(complete);  assert(dataValid);

        // 00001111 1 should be invalid for even parity
        #2 dataIn = 0;
        #2 assert(dataOut[  0] == 1'h00); assert(!complete); assert(!dataValid);
        #4 assert(dataOut[1:0] == 2'h00); assert(!complete); assert(!dataValid);
        #4 assert(dataOut[2:0] == 3'h00); assert(!complete); assert(!dataValid);
        #4 assert(dataOut[3:0] == 4'h00); assert(!complete); assert(!dataValid);
        #2 dataIn = 1;
        #2 assert(dataOut[4:0] == 5'h01); assert(!complete); assert(!dataValid);
        #4 assert(dataOut[5:0] == 6'h03); assert(!complete); assert(!dataValid);
        #4 assert(dataOut[6:0] == 7'h07); assert(!complete); assert(!dataValid);
        #4 assert(dataOut[7:0] == 8'h0f); assert(!complete); assert(!dataValid);
        #4 assert(dataOut[7:0] == 8'h0f); assert(complete);  assert(!dataValid);

        // 01010101 0 should be valid for even parity
        #2 dataIn = 0;
        #2 assert(dataOut[  0] == 1'h00); assert(!complete); assert(!dataValid);
        #2 dataIn = 1;
        #2 assert(dataOut[1:0] == 2'h01); assert(!complete); assert(!dataValid);
        #2 dataIn = 0;
        #2 assert(dataOut[2:0] == 3'h02); assert(!complete); assert(!dataValid);
        #2 dataIn = 1;
        #2 assert(dataOut[3:0] == 4'h05); assert(!complete); assert(!dataValid);
        #2 dataIn = 0;
        #2 assert(dataOut[4:0] == 5'h0a); assert(!complete); assert(!dataValid);
        #2 dataIn = 1;
        #2 assert(dataOut[5:0] == 6'h15); assert(!complete); assert(!dataValid);
        #2 dataIn = 0;
        #2 assert(dataOut[6:0] == 7'h2a); assert(!complete); assert(!dataValid);
        #2 dataIn = 1;
        #2 assert(dataOut[7:0] == 8'h55); assert(!complete); assert(!dataValid);
        #2 dataIn = 0;
        #2 assert(dataOut[7:0] == 8'h55); assert(complete);  assert(dataValid);

        // 10101010 1 should be invalid for even parity
        #2 dataIn = 1;
        #2 assert(dataOut[  0] == 1'h01); assert(!complete); assert(!dataValid);
        #2 dataIn = 0;
        #2 assert(dataOut[1:0] == 2'h02); assert(!complete); assert(!dataValid);
        #2 dataIn = 1;
        #2 assert(dataOut[2:0] == 3'h05); assert(!complete); assert(!dataValid);
        #2 dataIn = 0;
        #2 assert(dataOut[3:0] == 4'h0a); assert(!complete); assert(!dataValid);
        #2 dataIn = 1;
        #2 assert(dataOut[4:0] == 5'h15); assert(!complete); assert(!dataValid);
        #2 dataIn = 0;
        #2 assert(dataOut[5:0] == 6'h2a); assert(!complete); assert(!dataValid);
        #2 dataIn = 1;
        #2 assert(dataOut[6:0] == 7'h55); assert(!complete); assert(!dataValid);
        #2 dataIn = 0;
        #2 assert(dataOut[7:0] == 8'haa); assert(!complete); assert(!dataValid);
        #2 dataIn = 1;
        #2 assert(dataOut[7:0] == 8'haa); assert(complete);  assert(!dataValid);

        #2 $finish;
    end

endmodule
