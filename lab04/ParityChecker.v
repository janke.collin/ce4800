/**
 * 8-bit Serial Parity Checker
 *
 * Serially receives a symbol, checks its parity, and produces a parallel data
 * output. A symbol consists of 8 data bits transmitted MSB first followed by a
 * parity bit.
 *
 * In the clock cycle after the symbol is received:
 *   1. The `complete` signal will be set to 1.
 *   2. The `dataValid` signal will be set to 1 if the received symbol has
 *      correct parity.
 *   3. The data byte may be read from the `dataOut` bus.
 *
 * Parity is calculated by counting the number of '1's in the received symbol.
 * For even parity, the parity bit should be set so that the symbol has an even
 * number of '1's. For odd parity, the parity bit should be set so that the
 * symbol has an odd number of '1's.
 *
 * Inputs:
 *   dataIn         : Serial data input
 *   dataRecv       : Receive data enable (active high). While 0, the state of
 *                      the component will not change (unless rst_n is asserted).
 *   parity_evenOdd : Parity mode selection. Uses odd parity when 1, else even
 *                      parity. The parity mode used to check a byte is
 *                      determined by reading this signal at the same time as
 *                      the parity bit. While receiving the data bits, this line
 *                      is ignored.
 *   clk            : Clock (rising edge triggered)
 *   rst_n          : Asynchronous active low reset
 *
 * Outputs:
 *   dataOut        : Parallel data output (8 bits)
 *   complete       : Set to 1 when a full symbol (8 bits + parity) has been
 *                      received.
 *   dataValid      : When 1, the `dataOut` bus contains a valid byte with
 *                      correct parity (implies `complete=1`).
 *
 * Example:
 *
 * time      | 0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21
 * rst_n     |_-----------------------------------------------------------------
 * clk       |___---___---___---___---___---___---___---___---___---___---___---
 * parity    |__________________________________________________________________
 * dataIn    |______------______------____________------------______------______
 * dataOut   |00 |00   |01   |02   |05   |0a   |14   |29   |53   |53   |a7   |4e
 * complete  |___________________________________________________------_________
 * dataValid |___________________________________________________------_________
 *
 * In the rising edge before t=1, the first data bit is recorded, and on the
 * rising edges up until t=15, the data bits are shifted in. At t=17, the parity
 * bit is received, but is not shifted into the output. As the `complete` and
 * `dataValid` signals indicate, the symbol 01010011+0 has correct even parity,
 * so during t=17 and t=18, the `dataOut` value may be read.
 *
 * During t=18, `dataIn` has the MSB of the next symbol, and at t=19 it is
 * shifted into the data register. Note that the existing data value is simply
 * shifted left 1 position as the new data enters at t=19 and t=21.
 */
module ParityChecker (
    output reg complete,
    output reg dataValid,
    output reg [7:0] dataOut,
    input dataIn,
    input parity_evenOdd,
    input dataRecv,
    input clk, rst_n
);

    reg [3:0] bitsReceived;
    reg odd;

    always @(posedge clk, negedge rst_n) begin
        if (rst_n === 0) begin
            // Async reset
            complete = 0;
            dataValid = 0;
            dataOut = 0;
            bitsReceived = 0;
            odd = 0;
        end
        else if (dataRecv === 1) begin
            if (bitsReceived === 4'd8) begin
                 // Receiving parity bit, next will be data bit 0
                 complete = 1;
                 dataValid = (odd ^ dataIn) ~^ parity_evenOdd;
                 bitsReceived = 0;
                 odd = 0;
            end
            else begin
                // Receiving data bit
                complete = 0;
                dataValid = 0;
                dataOut = {dataOut[6:0], dataIn};
                bitsReceived = bitsReceived + 1;
                odd = odd ^ dataIn;
            end
        end
    end

endmodule
