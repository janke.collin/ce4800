module RCA_Test ();

    parameter N = 4;

    wire [N-1:0] sum;
    wire cout;
    reg [N-1:0] a;
    reg [N-1:0] b;
    reg cin;

    RCA #(.N(N)) uut (
        .sum(sum),
        .cout(cout),
        .a(a),
        .b(b),
        .cin(cin)
    );

    initial $monitor("%2d+%2d+%b=%2d+%2d (%4b+%4b+%b = %b%4b)",
        a, b, cin, sum, (cout ? 16 : 0),
        a, b, cin, cout, sum);

    initial #20 $finish;

    initial begin
        a = 0; b = 0; cin = 0;
        #1
        assert(sum === 0);
        assert(cout === 0);

        #1 a = 5; b = 3; cin = 0;
        #1
        assert(sum === 8);
        assert(cout === 0);

        #1 a = 0; b = 9; cin = 0;
        #1
        assert(sum === 9);
        assert(cout === 0);

        #1 a = 8; b = 7; cin = 0;
        #1
        assert(sum === 15);
        assert(cout === 0);

        #1 a = 7; b = 8; cin = 0;
        #1
        assert(sum === 15);
        assert(cout === 0);

        #1 a = 7; b = 8; cin = 1;
        #1
        assert(sum === 0);
        assert(cout === 1);

        #1 a = 7; b = 7; cin = 1;
        #1
        assert(sum === 15);
        assert(cout === 0);

        #1 a = 15; b = 15; cin = 1;
        #1
        assert(sum === 15);
        assert(cout === 1);
    end

endmodule
