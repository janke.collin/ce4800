module Lab01Adder_Test ();

    wire [3:0] sum;
    wire signed_overflow, unsigned_overflow;
    wire all_zero;

    assign all_zero = (sum === 0) && !signed_overflow && !unsigned_overflow;

    reg [3:0] a, b;
    reg addSubtract;
    reg enable_n;

    Lab01Adder UUT (
        .sum(sum),
        .signed_overflow(signed_overflow),
        .unsigned_overflow(unsigned_overflow),
        .a(a),
        .b(b),
        .addSubtract(addSubtract),
        .enable_n(enable_n)
    );

    initial begin
        #20 $finish;
    end

    initial begin
        enable_n = 1;
        addSubtract = 0;
        a = 0;
        b = 0;
        #1
        assert(all_zero);

        // Test 1: Zero when disabled
        #1 a = 1;
        #1
        assert(all_zero);

        // Test 2: Zero when disabled, unsigned overflow
        #1 b = 15;
        #1
        assert(all_zero);

        // Test 3: Zero when disabled, signed overflow
        #1 b = 7;
        #1
        assert(all_zero);

        // Test 4: 1 + 7 = 8, signed overflow
        #1 enable_n = 0;
        #1
        assert(sum === 8);
        assert(!unsigned_overflow);
        assert(signed_overflow);

        // Test 5: 1 + 15 = 16, unsigned overflow
        #1 b = 15;
        #1
        assert(sum === 0);
        assert(unsigned_overflow);
        assert(!signed_overflow);

        // Test 6: 8 + 15 = 23, signed and unsigned overflow
        //       (-8)+(-1)=(-9)
        #1 a = 8;
        #1
        assert(sum === 7) // 23 % 16 = 7, (-9) % 16 = 7
        assert(unsigned_overflow)
        assert(signed_overflow)

        // Test 7: 4 + 3 = 7, no overflow
        #1 a = 4;
        b = 3;
        #1
        assert(sum === 7);
        assert(!unsigned_overflow);
        assert(!signed_overflow);

        // Test 8: 4 - 3 = 1, unsigned overflow
        #1 addSubtract = 1;
        #1
        assert(sum === 1);
        assert(unsigned_overflow);
        assert(!signed_overflow);

        // Test 9: -3 - 6 = 7, signed and unsigned overflow
        // Signed overflow should be calculated from b_int, not b
        #1 a = 4'd1101; b = 6;
        #1
        assert(sum === 7);
        assert(unsigned_overflow);
        assert(signed_overflow);

    end

    initial $monitor("[%0t] %d %s %d = %d (e%b u%b s%b)",
        $time,
        a, (addSubtract ? "-" : "+"), b,
        sum,
        enable_n, unsigned_overflow, signed_overflow
    );

endmodule
