/**
 * A 4-bit 2's-complement ripple-carry adder/subtractor with overflow detection
 * and active-low enable
 *
 * Inputs:
 *   a: 4-bit operand A
 *   b: 4-bit operand B
 *   addSubtract: When 0, add A and B; when 1, subtract B from A.
 *       0: sum = a + b
 *       1: sum = a - b
 *   enable_n: Active-low enable. When 1, set all outputs to 0, regardless of
 *       input values.
 *
 * Outputs:
 *   sum: 4-bit sum or difference of A and B.
 *   signed_overflow: 1 if signed overflow occurred, else 0. For the purposes of
 *       this signal, A and B will always be treated as 2's-complement signed
 *       integers, regardless of whether the component is adding or subtracting.
 *   unsigned_overflow: 1 if unsigned overflow occurred (the carry-out of the
 *       adder was set), else 0.
 */
module Lab01Adder (
    output [3:0] sum,
    output signed_overflow, unsigned_overflow,
    input [3:0] a, b,
    input addSubtract,
    input enable_n
);

    wire cout;
    wire [3:0] b_int, sum_int;

    assign b_int = addSubtract ? ~b : b;

    RCA#(4) rca4 (
        .a(a),
        .b(b_int),
        .cin(addSubtract),
        .sum(sum_int),
        .cout(cout)
    );

    assign sum = enable_n ? 4'b0 : sum_int;
    assign unsigned_overflow = ~enable_n & cout;

    // Signed overflow if sign bits of A and B were the same, but sum has opposite sign
    assign signed_overflow = ~enable_n & (a[3] ~^ b_int[3]) & (a[3] ^ sum_int[3]);


endmodule
