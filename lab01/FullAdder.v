module FullAdder (
    output cout, // Carry-out
           sum,  // Sum
    input  a,    // Operand A
           b,    // Operand B
           cin   // Carry-in
);

    assign sum = a ^ b ^ cin;
    assign cout = (a & b) | (a & cin) | (b & cin);

endmodule
