module FullAdder_Test ();

    wire cout;
    wire sum;
    reg a;
    reg b;
    reg cin;

    FullAdder uut (
        .cout(cout),
        .sum(sum),
        .a(a),
        .b(b),
        .cin(cin)
    );

    initial $monitor("%b+%b+%b = %b%b",
        a, b, cin, cout, sum);

    initial #20 $finish;

    initial begin
        a = 0;
        b = 0;
        cin = 0;
        #1
        assert(sum === 0);
        assert(cout === 0);

        #1 a = 1; b = 0; cin = 0;
        #1
        assert(sum === 1);
        assert(cout === 0);

        #1 a = 0; b = 1; cin = 0;
        #1
        assert(sum === 1);
        assert(cout === 0);

        #1 a = 1; b = 1; cin = 0;
        #1
        assert(sum === 0);
        assert(cout === 1);


        #1 a = 0; b = 0; cin = 1;
        #1
        assert(sum === 1);
        assert(cout === 0);

        #1 a = 1; b = 0; cin = 1;
        #1
        assert(sum === 0);
        assert(cout === 1);

        #1 a = 0; b = 1; cin = 1;
        #1
        assert(sum === 0);
        assert(cout === 1);

        #1 a = 1; b = 1; cin = 1;
        #1
        assert(sum === 1);
        assert(cout === 1);
    end

endmodule
