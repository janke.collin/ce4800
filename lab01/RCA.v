/**
 * An N-bit ripple-carry adder.
 *
 * Calculates the N-bit sum of operands A and B plus 1 if the carry-in bit is
 * set. The multiple-bit sum is calculated by generating a full adder for each
 * bit and chaining the carry-out signal to the carry-in signal of the next
 * adder. A similar strategy could be used to connect multiple RCA components
 * together.
 */
module RCA
 #(parameter N = 4)
  (
    output         cout, // Carry-out bit
    output [N-1:0] sum,  // N-bit Sum
    input  [N-1:0] a,    // N-bit Operand A
                   b,    // N-bit Operand B
    input          cin   // Carry-in bit
);

    wire [N:0] c;       // carry chains
    assign c[0] = cin;  // cin of first adder comes from input signal
    assign cout = c[N]; // cout of last adder goes to output signal

    genvar i;
    generate
        for (i = 0; i < N; i = i + 1) begin : FullAdderGen
            FullAdder fa (
                .a(a[i]),
                .b(b[i]),
                .cin(c[i]),
                .sum(sum[i]),
                .cout(c[i+1])
            );
        end
    endgenerate

endmodule
