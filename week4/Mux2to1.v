module Mux2to1 (
    output y,
    input sel,
    input [1:0] a
);

    always @(sel, a) begin
        case (sel)
            0: y = a[0];
            1: y = a[1];
            default: y = 1'bx;
        endcase
    end

endmodule
