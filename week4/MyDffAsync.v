module MyDffAsync (
    output reg q,
    output q_n,
    input clk, en, set, clr, d
);

    assign q_n = !q;

    always @(posedge clk, posedge set, posedge clr) begin
        if (clr === 1)
            q <= 0;
        else if (set === 1)
            q <= 1;
        else if (en === 1)
            q <= d;
    end

endmodule
