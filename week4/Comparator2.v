module Comparator2
 #(parameter N = 32)
  (
    output lt, gt, eq, // in Verilog 2001, these need to be `output reg` since
                       // they're used in `always` (SystemVerilog doesn't care)
    input [N-1:0] a, b
);

    // assign lt = (a < b);
    // assign gt = (a > b);
    // assign eq = (a == b);

    always @(a, b) begin
        if (a == b)
        begin
            lt = 0;
            gt = 0;
            eq = 1;
        end
        else if (a < b)
        begin
            lt = 1;
            gt = 0;
            eq = 0;
        end
        else
        begin
            lt = 0;
            gt = 1;
            eq = 0;
        end
    end

endmodule
