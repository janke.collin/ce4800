module MyLatch (
    output q,
    input en, rst, d
);

    assign q = (rst === 1)
        ? 0
        : (en ? d : q);

endmodule
